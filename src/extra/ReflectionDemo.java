package extra;

import basics2.OuterClass;
import shared.Person;

import java.lang.reflect.Method;

public class ReflectionDemo {

    public static void main(String[] args) {
        Class<OuterClass> clz = OuterClass.class;

        printMethods(clz,"");
        for(Class c : clz.getClasses())
        {
           printMethods(c,"\t");
        }
    }

    static void printMethods(Class c, String level)
    {
        System.out.println(level+ "Methods for the class :"+c.getSimpleName());
        for(Method m: c.getMethods())
            System.out.println(level+"\t" + m.getName());

    }
}
