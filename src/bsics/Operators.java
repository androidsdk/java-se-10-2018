package bsics;

import java.util.Locale;

public class Operators {

    public static void main(String[] args) {
        /**
         * + , - ,*, /, % , < , >, <=, >= , =, == , != , ++ , -- , (-) , +=, -= , &&, ||,!
         *  & (BITWISE AND),
         *  | BITWISE OR),
         *  ^ BITWISE XOR),
         *  << (left SHIFT),
         *  >> (RIGHT SHIFT),
         *  >>> (zero filled right shift),
         *  ~ (complement),
         */


        int a = 10, b = 3;

        // Addition (+)
        System.out.println(10 + 3 + 2 );                // 15
        System.out.println("10" + 3 + 2 );              // 1032,
        System.out.println(10 + "3" + 2 );              // 1032
        System.out.println(10 + 3 + "2" );              // 132

        System.out.println("a + b =" + a + b);          // a + b =103
        System.out.println("a + b =" + (a + b));          // a + b =13

        System.out.println("5.3 + 5 =" + (5.3 + 5));
        System.out.println("'A' + 'B' =" + ('A' + 'B'));


        System.out.println("a % b =" + (a % b));            // 1
        System.out.println("10.5 % b =" + (10.5 % b));      // 1.5
        System.out.println("10.5 % 3.5 =" + (10.5 % 3.5));  // 0

        System.out.println(a == b);     // false
        //  System.out.println(a = b);      // 3
        System.out.println(a % b);      // 1


        a = 15 ;
        System.out.println(a++);         // 10 then add 1 to a to a be 11
        System.out.println(a);           // 11
        System.out.println(++a);         // add 1 to a (was 11) to be 12 then print the new value 12


        // a  = 12, b = 3
        System.out.println(a == b);     // means is a equals to b (true/false)
        System.out.println(a != b);
        System.out.println(a > b);
        System.out.println(a < b);
        System.out.println(a <= b);
        System.out.println(a >= b);

         //System.out.println( a && b );          // Error: because && , || , ! accepts boolean operands only

        System.out.println(a > 10 && b > 10);    //

        System.out.println(!(false && true));

        System.out.printf(Locale.getDefault(),"%d & %d = %d%n",a,b, (a & b));

        a = 10;
        b = 3;

        System.out.printf(Locale.getDefault(),"%d & %d = %d%n",a,b, (a & b));       // 2
        System.out.printf(Locale.getDefault(),"%d & %d = %d%n",a,b, (a | b));       //
        System.out.printf(Locale.getDefault(),"%d & %d = %d%n",a,b, (a ^ b));



        int r = a;
        r *= b + 2;       // r  = r* (b+ 2)
        System.out.println("R: "+ r);

        a = 2;
        b = 3;

        int c = 3 * a - b * 2 * (a + b) / 3;
        System.out.println("First c :" + c);


        c = 3 * a++ - b++ * 2 * (a++ + b++) / 3;
        System.out.println("Second c :" + c);


        //TODO: Operators  27/05/2018

        a = -10;         //  00000000001010
        b = 3;         //

        Locale locale = Locale.getDefault();
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a", a, String.format(locale, "%32s", Integer.toBinaryString(a)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "b", b, String.format(locale, "%32s", Integer.toBinaryString(b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a & b", a & b, String.format(locale, "%32s", Integer.toBinaryString(a & b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a | b", a | b, String.format(locale, "%32s", Integer.toBinaryString(a | b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a ^ b", a ^ b, String.format(locale, "%32s", Integer.toBinaryString(a ^ b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "~a", ~a, String.format(locale, "%32s", Integer.toBinaryString(~a)).replaceAll(" ", "0"));

        System.out.println("----------- shift left  <<   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 0", a << 0, String.format(locale, "%32s", Integer.toBinaryString(a << 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 1", a << 1, String.format(locale, "%32s", Integer.toBinaryString(a << 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 2", a << 2, String.format(locale, "%32s", Integer.toBinaryString(a << 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 3", a << 3, String.format(locale, "%32s", Integer.toBinaryString(a << 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 4", a << 4, String.format(locale, "%32s", Integer.toBinaryString(a << 4)).replaceAll(" ", "0"));

        System.out.println("-----------  shift right >>   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 0", a >> 0, String.format(locale, "%32s", Integer.toBinaryString(a >> 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 1", a >> 1, String.format(locale, "%32s", Integer.toBinaryString(a >> 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 2", a >> 2, String.format(locale, "%32s", Integer.toBinaryString(a >> 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 3", a >> 3, String.format(locale, "%32s", Integer.toBinaryString(a >> 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 4", a >> 4, String.format(locale, "%32s", Integer.toBinaryString(a >> 4)).replaceAll(" ", "0"));

        System.out.println("-----------  zero filled shift right >>>   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 0", a >>> 0, String.format(locale, "%32s", Integer.toBinaryString(a >>> 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 1", a >>> 1, String.format(locale, "%32s", Integer.toBinaryString(a >>> 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 2", a >>> 2, String.format(locale, "%32s", Integer.toBinaryString(a >>> 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 3", a >>> 3, String.format(locale, "%32s", Integer.toBinaryString(a >>> 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 4", a >>> 4, String.format(locale, "%32s", Integer.toBinaryString(a >>> 4)).replaceAll(" ", "0"));


        a = 10;
        System.out.println(a < 0 && ++a != 10);         // false
        System.out.println(a);                          // 10      , because with logical operator (&&,||)  uses the short circuit

        a = 10;
        System.out.println(a < 0 & a++ != 10);          // false
        System.out.println(a);                          // 11

    }
}
