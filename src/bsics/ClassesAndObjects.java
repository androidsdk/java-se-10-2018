package bsics;

public class ClassesAndObjects {

    static  class Point {
        int x , y ,z ;
        public  void changeX(int a)
        {
            x += a ;
        }
    }
    // class : a description / template/ blueprint for a data type,
    //  that defines its states (variables/ objects) and behaviours (methods)

    // an object : an instance of a class

    // a variable : a named reference for a memory location

    // The Object class :
    public static void main(String[] args) {
        Point p ;
        p = new Point();
        System.out.println("p.x = " + p.x);
        p.x = 5 ;
        System.out.println("p.x = " + p.x);
        p.changeX(10);
        System.out.println("p.x = " + p.x);
        Object o  = new Object();
        Point[] points = new Point[20];             //
        for(int i=0;i<points.length;i++)
            points[i] = new Point();
    }

}
