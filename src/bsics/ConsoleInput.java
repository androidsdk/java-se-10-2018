package bsics;

import java.util.Locale;
import java.util.Scanner;

public class ConsoleInput {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.getDefault());
        System.out.print("Your age:");
        int age = scanner.nextInt();
        double avg = scanner.nextDouble() ;

        System.out.println("age = " + age);
        System.out.println("avg = " + avg);

        System.out.println();

    }
}
