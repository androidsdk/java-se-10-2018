package bsics;

import java.util.Locale;
import java.util.Random;

public class MultiDimensionArrays {

    private static final int FIRST_DIMENSION = 3;
    private static final int SECOND_DIMENSION = 5;
    private static final int THIRD_DIMENSION =  4;

    public static void main(String[] args) {
        // 2D, 3d ,4D ....
        // nD array is an arrays of (n-1)D arrays
        // 2D is an array of 1D arrays
//        int[] arr0 = new int[5];
//        int[] arr1 = new int[5];
//        int[] arr2 = new int[5];

        int counter = 0 ;
        int[][] mat = new int[FIRST_DIMENSION][SECOND_DIMENSION] ;

        for (int r= 0;r <mat.length;r++)
        {
            for(int c=0;c<mat[r].length;c++)
            {
                mat[r][c] = counter++ ;
                System.out.printf(Locale.getDefault(),"%5d,",mat[r][c]);
            }
            System.out.println("\b");
        }

        System.out.println("=============3D array===================");
        int[][][] ThreeD = new int[FIRST_DIMENSION][SECOND_DIMENSION][THIRD_DIMENSION];

        counter = 0 ;
        for (int r= 0;r <ThreeD.length;r++)
        {
            for(int c=0;c<ThreeD[r].length;c++)
            {
                for(int d=0;d< ThreeD[r][c].length;d++) {
                    ThreeD[r][c][d] = counter++;
                    System.out.printf(Locale.getDefault(), "%5d,",  ThreeD[r][c][d]);
                }
                System.out.println("\b");
            }
            System.out.println("+++++++++++++++++++++++++++++++++");
        }


        System.out.println("=============Jagged array===================");
        Random random = new Random();
        int[][][] Jagged = new int[1 + random.nextInt(10)][][];
        for(int i=0;i<Jagged.length;i++)
        {
            Jagged[i] = new int[1 + random.nextInt(10)][] ;
            for(int j=0;j<Jagged[i].length;j++)
                Jagged[i][j] = new int[1 + random.nextInt(10)];            //  Jagged[i][j] = new int[j+1];
        }

        counter = 0 ;
        for (int r= 0;r <Jagged.length;r++)
        {
            for(int c=0;c<Jagged[r].length;c++)
            {
                for(int d=0;d< Jagged[r][c].length;d++) {
                    Jagged[r][c][d] = counter++;
                    System.out.printf(Locale.getDefault(), "%5d,",  Jagged[r][c][d]);
                }
                System.out.println("\b");
            }
            System.out.println("+++++++++++++++++++++++++++++++++");
        }
    }
}
