package bsics;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class Arrays {

    // Arrays : set of consecutive memory locations that hold values of the same type
    public static void main(String[] args) {

        Random random = new Random();
       int[] marks1 = {85,75,94,35};

        System.out.println(marks1);

        int[] marks = new int[30];
        int maxIndex = 0 , minIndex = 0 ;
        System.out.println("marks = " + marks);
        System.out.print("{");
        for(int i=0;i<marks.length;i++)
        {
           // marks[i] =  35 + random.nextInt(66) ;
            marks[i] =  35 + (int)(random.nextDouble()* 66) ;
            if(marks[i] > marks[maxIndex])
                maxIndex = i ;

            if(marks[i] < marks[minIndex])
                minIndex = i ;

            System.out.print(marks[i] + ",");
            //System.out.println("marks["+i+"] = " + marks[i]);
           // System.out.printf(Locale.getDefault(),"marks[%d] = %d%n",i, marks[i]);
        }
        System.out.println("\b}");
        System.out.println("marks[maxIndex] = " + marks[maxIndex]);
        System.out.println("marks[minIndex] = " + marks[minIndex]);

    }
}
