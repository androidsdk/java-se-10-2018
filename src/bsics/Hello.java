package bsics;

public class Hello {
    public static void main(String[] args) {
        System.out.println("bsics.Hello");

        int a = 10 ;
        System.out.println("a = " + a);         // 10
        a = 010 ;                                       // any number start with 0 is octal
        System.out.println("a = " + a);         // 010
        a = 0X10ABCDEF ;                                       // any number start with 0x is hexadecimal
        System.out.println("a = " + a);         // 0X10
        a = 'A' ;
        System.out.println("a = " + a);         // A                // ASCII code
        a = '\u0041' ;
        System.out.println("a = " + a);         // 010              // Unicode  (16 bit)
        a = 0101 ;
        System.out.println("a = " + a);         // 010
    }
}
