package bsics;

public class StringsDemo {
    public static void main(String[] args) {

        // string is an array of characters
        int a = 5, b = 5 ;
        System.out.println("(a== b) = " + (a== b));
        String str0 = "this is a string";
        String str1 = "this is a string";
        String str2 = new String("this is a string");
        String str3 = new String("this is a string");


        //str0 = "string" ;
        System.out.println("str0 = " + str0);
        System.out.println("str1 = " + str1);
        System.out.println("str2 = " + str2);
        System.out.println("str3 = " + str3);

        System.out.println("(str0 == str1) = " + (str0 == str1));       // true
        System.out.println("(str0 == str2) = " + (str0 == str2));       // false
        System.out.println("(str2 == str3) = " + (str2 == str3));       // false

        System.out.println("(str1.equals(str0)) = " + (str1.equals(str0)));
        System.out.println("(str0.equals(str2)) = " + (str0.equals(str2)));
        System.out.println("(str2.equals(str3)) = " + (str2.equals(str3)));

        System.out.println("(str1.equalsIgnoreCase(str0)) = " + (str1.equalsIgnoreCase(str0)));
        System.out.println("(str0.equalsIgnoreCase(str2)) = " + (str0.equalsIgnoreCase(str2)));
        System.out.println("(str2.equalsIgnoreCase(str3)) = " + (str2.equalsIgnoreCase(str3)));


        System.out.println("str0.replaceAll(\"s\",\"S\") = " + str0.replaceAll("s","S"));
        System.out.println("str0 = " + str0);
        System.out.println("(str0.substring(5)) = " + (str0.substring(5)));
        System.out.println("(str0.substring(5,8)) = " + (str0.substring(5, 8)));
        System.out.println("(\"ABC\".compareTo(\"ABC\")) = " + ("ABC".compareTo("ABC")));
        System.out.println("(\"aBC\".compareTo(\"ABD\")) = " + ("aBC".compareTo("ABD")));
        System.out.println("(\"ABC\".compareTo(\"aBC\")) = " + ("ABC".compareTo("aBC")));
        System.out.println("(\"ABCD\".compareTo(\"aBC\")) = " + ("ABCD".compareTo("aBC")));
        System.out.println("(\"ABC\".compareTo(\"ABCD\")) = " + ("ABC".compareTo("ABCD")));


        // TODO : 1) ask the user to enter a string
        // TODO: 2) check if the string is reversible
        // TODO:  reversible string is the string that can be read from either directions
        // TODO : Notes : ignore case sensitive letters and leading and trailing spaces
        // Examples :
        /*
                "A"  -->  yes
                "Ab" -->  NO
                "Aba" --> YEs
                "A "  -->  yes
                "  AbBa" -->  Yes
                "Aba" --> YEs

        */


    }
}
