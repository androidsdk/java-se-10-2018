package bsics;

import java.util.Scanner;

public class ControlOfFlow {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final int c = 1;
        int a = scanner.nextInt();

        /*
                syntax :
                        switch( <compatible_variable>|<compatible_expression>)
                        {
                            [case <unique_constant_compatible_value>:  [statements]* ]*
                            [default: [statements]* ]
                        }

                        compatible_variable : char, byte, short, int , Character, Byte, Short, Integer, String or enum
         */

        a = 1;
        final int m = 2;
        switch (a) {
            default:
                System.out.println("Default");
                break;

            case 1:
            case m:         // you can use final variables
            case m + 1:     // you can use final expressions
                System.out.println();

        }

        switch (a) {
            case 0:
            case c:
                System.out.println("One");
                break;
            case 2:
                System.out.println("zero");
                break;
            default:
                System.out.println("default");
        }
        //a = 2 ;

        if (a == 2) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }


        System.out.println("Done");

        System.out.println(a >= 0 ? a : -a);
        int b = a >= 0 ? a : -a;
        System.out.println("b = " + b);

        System.out.println("mark:");
        double mark = scanner.nextDouble();

        if (mark < 35 || mark > 100)
            System.out.println("Error");
            // 35 <= mark <= 100
        else if (mark < 50)
            System.out.println("Failed");
            // 50 <= mark <= 100
        else if (mark < 68)
            System.out.println("Pass");
            // 68 <= mark <= 100
        else if (mark < 76)
            System.out.println("Good");
            // 76 <= mark <= 100
        else if (mark < 84)
            System.out.println("Very good");
            // 84 <= mark <= 100
        else
            System.out.println("Excellent");
    }
}
