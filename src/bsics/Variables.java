package bsics;

public class Variables {

    public static void main(String[] args) {

        // bsics.Variables : name for a memory location reference

        // Syntax :  [specifier] [modifier] DATA_TYPE <variable_id> [= <expression>][, <variable_id> [= <expression>]]* ;

        // DATA_TYPE :
        //              1) Primitive data types  (value types)

        //                  bool (zero/non-zero)   , char       , short, int, long , float , double         in c++
        //                  boolean (true/false)  , char , byte, short, int, long , float , double      int java
        //              2) Non Primitive (reference type)


        boolean bool = false ;
        char ch= 'A';
        System.out.println("ch = " + ch);           // A
        ch= '\u0041';
        System.out.println("ch = " + ch);           // A
        ch= 65;
        System.out.println("ch = " + ch);           // A
        ch= 0101;
        System.out.println("ch = " + ch);           // A
        ch= 0x41;
        System.out.println("ch = " + ch);           // A

        byte by = -128 ; // 127
        short sh = -32768 ;
        int integ = -214748364 ;
        long lon = -9223372036854775807L ;

        sh = by ;
        by = (byte) sh;
        sh = (short) integ;
        System.out.println("sh = " + sh);

        float flo = 15.326F ;
        double dou = 15.326 ;
        
        // Type conventions :
        //  we can convert byte --> short--> int--> long --> float -->double
        // Risky cases :
        //      1) int--> float : for big integer value
                int value = 2147483647 ;
                float target = value ;

                System.out.println("value  = " + value);
                System.out.println("target = " + target);

        //      2) long --> float : for big long value
        long longValue = Long.MAX_VALUE ;
         target = longValue ;

        System.out.println("longValue  = " + longValue);
        System.out.println("target     = " + target);
        //      3) long --> double : for big long value

        double  dubTraget = longValue ;

        System.out.println("longValue  = " + longValue);
        System.out.println("dubTraget     = " + dubTraget);


        int a = (int) 15.65;

    }
}
