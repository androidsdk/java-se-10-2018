package bsics;

import java.util.Locale;
import java.util.Random;

public class MoreAboutMethods {

    static void foo()
    {
        System.out.println("Foo()");
    }
    static void foo(int a)
    {
        a += 10 ;
        System.out.printf(Locale.getDefault(),"Foo(%d)%n",a);
    }
    static void foo(int[] a)
    {
        for(int i=0;i<a.length;i++) {
            a[i] += 10;
            System.out.printf(Locale.getDefault(), "Foo(%d)%n", a[i]);
        }
    }
    static void foo1(int[] a)
    {
        int [] temp = new int[a.length];
        for(int i=0;i<a.length;i++) {
            temp[i] = a[i]+ 10 ;
            System.out.printf(Locale.getDefault(), "Foo(%d)%n",  temp[i]);
        }
    }

    static  class  Holder {
        int a ;
    }
    static void foo(Holder h)
    {
        h.a += 10 ;
        System.out.printf(Locale.getDefault(),"Foo(%d)%n", h.a);
        return; // this will end the method
    }


    static int sum(int a, int b)
    {
        return  a+ b ;
    }

    static int  findMin(int[] array)
    {
        int min= Integer.MAX_VALUE;
        for(int i=0;i<array.length;i++)
        {
            if(array[i] < min)
                min = array[i];
        }

        return min ;
    }
    static int findMax(int[] array)
    {
        int max= Integer.MIN_VALUE;
        for(int i=0;i<array.length;i++)
        {
            if(array[i] > max)
                max = array[i];
        }

        return max ;

    }
    static int[] findMinMax(int[] array)
    {
        int[] minmax=  {Integer.MAX_VALUE,Integer.MIN_VALUE };

        for(int i=0;i<array.length;i++)
        {
            if(array[i] > minmax[1])
                minmax[1] = array[i];
            if(array[i] < minmax[0])
                minmax[0] = array[i];
        }

        return  minmax ;

    }

    static class MinMax {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE ;

    }

    static MinMax findMinMax1(int[] array)
    {
        MinMax mm = new MinMax();

        for(int i=0;i<array.length;i++)
        {
            if(array[i] > mm.max)
                mm.max = array[i];
            if(array[i] < mm.min)
                mm.min = array[i];
        }
        return  mm ;
    }

    public static void main(String[] args) {

        foo();
        foo(100);
        int  c = 20 ;
        foo(c);
        System.out.println("c = " + c);
        System.out.println("--------------");
        int[] arr = {5,8,9};
        foo(arr);
        System.out.println(arr[0]+ ", "+arr[1]+ ", "+arr[2]);
        System.out.println("--------------");
        foo1(arr);
        System.out.println(arr[0]+ ", "+arr[1]+ ", "+arr[2]);
        System.out.println("--------------");

        Holder holder = new Holder();
        holder.a =50 ;
        foo(holder);
        System.out.println("holder.a = " + holder.a) ;

        int[] marks = new int[20];
        Random r = new Random();
        System.out.print("{");
        for(int i=0;i<marks.length;i++)
        {
            marks[i] = 35 + r.nextInt(66) ;
            System.out.printf(Locale.getDefault(),"%3d, ",marks[i]);
        }
        System.out.print("\b\b} \n");

        System.out.println("findMin(marks) = " + findMin(marks));
        System.out.println("findMax(marks) = " + findMax(marks));
        System.out.println("-----------------");
        int[] result = findMinMax(marks);
        System.out.println("Max = " + result[1]);
        System.out.println("Min = " + result[0]);
        System.out.println("-----------------");
        MinMax res = findMinMax1(marks);
        System.out.println("Max = " + res.max);
        System.out.println("Min = " + res.min);
    }
}
