package bsics;

public class MethodsDemo {
    // Method: named block of statements that has a (output) return type and optional
    // inputs (parameter list)
    // All method in java must be written inside a class
    // Syntax : 1) definition
            // <return_data_type> <method_name>([parameter list]) { [body]}
    //      2) call method      [<variable_name>/<class_name>.]method_name([argument_list]);
    //


    {
        // this is a block
    }

   static  class MyClass {
        int a ;
        static  int m ;
        void method()
        {
            System.out.println("This is a non method which is exist in each object of the class ");
        }
        static  void staticMethod()
        {
            System.out.println("This is a static method which is exist in the class itself");
        }
   }

    public static void main(String[] args) {

        MyClass obj = new MyClass();
        obj.a = 10 ;
        obj.method();
        // method();

        MyClass.staticMethod();
        MyClass.m = 20 ;

    }

}
