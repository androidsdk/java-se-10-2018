package threading;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class ThreadPoolDemo {

    static class Summer {
        private int sum;

        public void addToSum(int value) {
            setSum(getSum() + value);
        }

        public int getSum() {
            return sum;
        }

        public void setSum(int sum) {
            this.sum = sum;
        }
    }

    public static void main(String[] args) {

        int count = 100;
        Summer summer = new Summer();
        Random random = new Random();
        ExecutorService service = new ScheduledThreadPoolExecutor(4);
//        service.execute(()-> IntStream.range(1,2 + random.nextInt(count)).forEach(c -> summer.addToSum(c)));
//        service.execute(()-> IntStream.range(1,2 + random.nextInt(count)).forEach(c -> summer.addToSum(c)));
//        service.execute(()-> IntStream.range(1,2 + random.nextInt(count)).forEach(c -> summer.addToSum(c)));
//        service.execute(()-> IntStream.range(1,2 + random.nextInt(count)).forEach(c -> summer.addToSum(c)));
//        service.execute(()-> IntStream.range(1,2 + random.nextInt(count)).forEach(c -> summer.addToSum(c)));
//        service.execute(()-> IntStream.range(1,2 + random.nextInt(count)).forEach(c -> summer.addToSum(c)));
//
//       service.shutdown();
//        while (!service.isTerminated());
//            System.out.println("summer.getSum() = " + summer.getSum());

        // if you want to return a result

        ArrayList<Future<Integer>> futures = new ArrayList<>();
        Callable<Integer> callable = () -> IntStream.range(1, 2 + random.nextInt(count)).sum();
        for (int i = 0; i < 10; i++) {
            futures.add(service.submit(callable));
        }

        try {
            service.awaitTermination(1, TimeUnit.MINUTES);

            futures.forEach(f -> {
                try {
                    System.out.println(f.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //service = new ThreadPoolExecutor(2,4,1, TimeUnit.MINUTES,)
    }
}
