package threading;

import java.util.Locale;

public class Threading1 {

    private static int Counter = 10000;

    static int x = 0;

    public static void main(String[] args) {
        long start = System.nanoTime();
        loop(Counter);

        long end = System.nanoTime() - start;
        System.out.printf(Locale.getDefault(), "Time 1 = %10d%n", end);

        start = System.nanoTime();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+ " is started");
                for (int i = 0; i < Counter; i++)
                    x++;
               // System.out.println("Finish 1");
            }
        },"First thread");
        t.start();
        //end = System.nanoTime() - start;
        //System.out.printf(Locale.getDefault(), "Time 2 = %10d%n", end);

        start = System.nanoTime();
        Thread t1 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+ " is started");
            for (int i = 0; i < Counter; i++)
                x--;

           // System.out.println("Finish 2");
        },"Second thread");
        t1.start();
        try {
            t.join();
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        end = System.nanoTime() - start;


        System.out.printf(Locale.getDefault(), "Time 2 = %10d%n", end);

        System.out.println(Thread.currentThread().getName()+"-> x = " + x);
    }

    public static void loop(int counter) {
        for (int i = 0; i < counter; i++)
            x++;
        for (int i = 0; i < counter; i++)
            x--;
    }
}
