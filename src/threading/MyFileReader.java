package threading;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.ToIntFunction;

public class MyFileReader extends Thread {
    private Path mFile;

    public MyFileReader(String filepath) {
       this(Paths.get(filepath));
    }

    public MyFileReader(File file) {
      this(file.toPath());
    }

    public MyFileReader(Path filepath) {
        mFile = filepath;
        start();
    }


    @Override
    public void run() {

         AtomicInteger lines = new AtomicInteger();

        String fileName = mFile.getFileName().toString();
        Random rand = new Random();

        try (BufferedReader reader = new BufferedReader(new FileReader(mFile.toFile()))) {

            Thread.sleep(1000 + rand.nextInt(10*1000));
           // System.out.println("\n========== Start reading file :" + fileName + "==========\\n");
           // reader.lines().forEach(l -> System.out.printf(Locale.getDefault(),"%-10d)%-20s:%s%n", lines.getAndIncrement(),fileName,l));
            // OR
            //reader.lines().forEach(System.out::println);
            //reader.lines().mapToInt(l-> l.split(" ").length).sum();
            //System.out.printf(Locale.getDefault(),"Number of lines in file: \"%s\"= %d%n" ,fileName, reader.lines().count());

            ToIntFunction<String> function = new ToIntFunction<String>() {
                @Override
                public int applyAsInt(String l) {
                  //  String[] sp =  l.replaceAll("\"","").split("(,| )") ;
                    String[] sp =  l.split("( )") ;
                    return (int) Arrays.stream(sp).filter(w->w.length()>0).count();
                }
            };

//            reader.lines().mapToInt(function).forEach(System.out::println);

//            reader.lines().map(l-> Arrays.stream(l.replace("\"", "").split("(,| )")))
//                    .
//                    .forEach(System.out::println);
            System.out.printf(Locale.getDefault(),"Number of word in file: \"%s\"= %d%n" ,fileName,  reader.lines().mapToInt(function).sum());


           // System.out.println("\n========== End reading file :" + fileName + "==========\\n");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
