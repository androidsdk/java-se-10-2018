package threading;

import shared.TimeHelper;

import java.util.Locale;

public class ThreadingStart {
   long value ;
    long x ;
    Boolean ValueLock , XLock ;

    public  void increase(long count) {
//        System.out.printf(Locale.getDefault(), "Thread :%-30s, method:%-10s, counter:%5d%n", Thread.currentThread().getName(), "increase", count);

        synchronized (XLock)
        {
            x++ ;
        }
        for (int i = 0; i < count; i++) {
            synchronized (ValueLock) {
                value++;
            }
            // System.out.printf(Locale.getDefault(),"Thread :%-30s, method:%-10s, value:%5d%n",Thread.currentThread().getName(),"decrease",value);

        }
    }

    public  void decrease(long count) {
//        System.out.printf(Locale.getDefault(), "Thread :%-30s, method:%-10s, counter:%5d%n", Thread.currentThread().getName(), "decrease", count);
        for (int i = 0; i < count; i++) {
            synchronized (ValueLock) {
                value--;
            }
            // System.out.printf(Locale.getDefault(),"Thread :%-30s, method:%-10s, value:%5d%n",Thread.currentThread().getName(),"decrease",value);
        }
    }

    public static void main(String[] args) {

        long c = 10000000;
        int runs = 360;
        ThreadingStart threadingStart = new ThreadingStart();

        long withThreaingWithoutlamndaTime = 0, noThreadingTime = 0, withThreadingAndlambdaTime = 0;
        for (int i = 0; i < runs; i++) {
            // this switch to run the methods in different orders to avoid any order of execution related changes
            switch (i % 6) {
                case 0:
                    withThreaingWithoutlamndaTime += withThreadingWithoutlambda(threadingStart, c);
                    noThreadingTime += withOutThreading(threadingStart, c);
                    withThreadingAndlambdaTime += withThreadinglambda(threadingStart, c);
                    break;
                case 1:
                    withThreaingWithoutlamndaTime += withThreadingWithoutlambda(threadingStart, c);
                    withThreadingAndlambdaTime += withThreadinglambda(threadingStart, c);
                    noThreadingTime += withOutThreading(threadingStart, c);
                    break;
                case 2:
                    withThreadingAndlambdaTime += withThreadinglambda(threadingStart, c);
                    withThreaingWithoutlamndaTime += withThreadingWithoutlambda(threadingStart, c);
                    noThreadingTime += withOutThreading(threadingStart, c);
                    break;
                case 3:
                    withThreadingAndlambdaTime += withThreadinglambda(threadingStart, c);
                    noThreadingTime += withOutThreading(threadingStart, c);
                    withThreaingWithoutlamndaTime += withThreadingWithoutlambda(threadingStart, c);
                    break;
                case 4:
                    noThreadingTime += withOutThreading(threadingStart, c);
                    withThreadingAndlambdaTime += withThreadinglambda(threadingStart, c);
                    withThreaingWithoutlamndaTime += withThreadingWithoutlambda(threadingStart, c);

                    break;
                case 5:
                    noThreadingTime += withOutThreading(threadingStart, c);
                    withThreaingWithoutlamndaTime += withThreadingWithoutlambda(threadingStart, c);
                    withThreadingAndlambdaTime += withThreadinglambda(threadingStart, c);
                    break;
            }

        }
        System.out.printf(Locale.getDefault(), "No Threading            :%20s%n", TimeHelper.timeToString(noThreadingTime / runs));
        System.out.printf(Locale.getDefault(), "Threading with lambda   :%20s%n", TimeHelper.timeToString(withThreadingAndlambdaTime / runs));
        System.out.printf(Locale.getDefault(), "Threading without lambda:%20s%n", TimeHelper.timeToString(withThreaingWithoutlamndaTime / runs));

    }

    private static long withOutThreading(ThreadingStart obj, long c) {
        long start = System.nanoTime();
//        System.out.println("threadingStart.value = " + threadingStart.value);       // 0
        obj.increase(c);
//        System.out.println("threadingStart.value = " + threadingStart.value);       // 100
        obj.decrease(c);

//        System.out.println(Thread.currentThread().getName() + "->ThreadingStart.value = " + obj.value);       // 0

        return System.nanoTime() - start;
    }

    private static long withThreadinglambda(ThreadingStart obj, long c) {
        long start = System.nanoTime();
        Thread t1 = new Thread(() -> obj.increase(c), "First thread");
        Thread t2 = new Thread(() -> obj.decrease(c), "Second thread");
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        System.out.println(Thread.currentThread().getName() + "->ThreadingStart.value = " + obj.value);       // 0
        return System.nanoTime() - start;
    }

    private static long withThreadingWithoutlambda(ThreadingStart obj, long c) {
        long start = System.nanoTime();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                obj.increase(c);
            }
        }, "First thread");
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                obj.decrease(c);
            }
        }, "Second thread");
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        System.out.println(Thread.currentThread().getName() + "->ThreadingStart.value = " + obj.value);       // 0
        return System.nanoTime() - start;
    }
}
