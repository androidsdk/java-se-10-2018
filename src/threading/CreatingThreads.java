package threading;

public class CreatingThreads {
    public static void main(String[] args) {

        
        String str = "\"ID\",\"Name\",\"PClass\",\"Age\",\"Gender\" ,\"Survived\",\"SexCode\"";
        String[] sp = str.replaceAll("\"","").split("(,| )");
        for(String s : sp)
            System.out.println("s = " + s);

        System.out.println("sp.length = " + sp.length);
        // Way 1

        Thread thread = new Thread(()-> {
            // TODO: add your code here to be executed in the thread
            System.out.println(Thread.currentThread().getName()+ "-> this is the run 1");
        },"First thread");
        thread.start();         // don't forget to start the thread to start working


        // way 2

        MyTask myTask = new MyTask();
        Thread thread1 = new Thread(myTask,"Second thread");
        thread1.start();


        // way 3
        MyWorkerThread thread2 = new MyWorkerThread();
       // thread2.start();


       MyFileReader reader = new MyFileReader("test.txt");
////        try {
////            reader.join();
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        }
//        MyFileReader reader1 = new MyFileReader("Test1.txt");
    }

    static class MyWorkerThread extends Thread {

        public MyWorkerThread() {
            super("MyWorker Thread");
            // [optional] you can start the thread automatically after create an object
             start();
        }

        @Override
        public void run() {
            // TODO: add your code here to be executed in the thread
            System.out.println(Thread.currentThread().getName()+ "-> this is the run 3");
        }
    }
    static class MyTask implements Runnable {

        @Override
        public void run() {
            // TODO: add your code here to be executed in the thread
            System.out.println(Thread.currentThread().getName()+ "-> this is the run 2");
        }
    }
}
