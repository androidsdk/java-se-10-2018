package threading;

import java.util.concurrent.*;
import java.util.stream.IntStream;

public class SynchronizedDemo {

    /**
     * The synchronized keyword can be used on different levels:
     *
     * 1)Instance methods
     *       synchronized void method() { }
     * 2) Static methods
     *      static synchronized void method() {}
     * 3) Code blocks
     *      synchronized (<object>) { }
     */

    private Integer sum = 0  ;
    Boolean SumLock ;

    public void addToSum(int value)
    {
        System.out.println(Thread.currentThread().getName() + ", value :"+ value);
        synchronized (sum) {
            setSum(getSum() + value);
        }
    }
    public   int getSum() {
        return sum;
    }

    public  void setSum(int sum) {
        this.sum = sum;
    }

    static  int tc = 1 ;
    public static void main(String[] args) {

        SynchronizedDemo obj = new SynchronizedDemo();
        int counter = 11 ;

        ExecutorService service = new ScheduledThreadPoolExecutor(3,r -> new Thread(r,"Thread "+tc++));
        service.submit(()-> IntStream.range(0,counter).forEach(c->obj.addToSum(c)));
        service.submit(()-> IntStream.range(0,counter).forEach(c->obj.addToSum(c)));
        service.submit(()-> IntStream.range(0,counter).forEach(c->obj.addToSum(c)));
//        service.submit(()-> IntStream.range(0,counter).forEach(c->obj.setSum(c)));
//        service.submit(()-> IntStream.range(0,counter).forEach(c->obj.setSum(c)));
        try {
            service.awaitTermination(30, TimeUnit.SECONDS);

            System.out.println("obj = " + obj.getSum());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
