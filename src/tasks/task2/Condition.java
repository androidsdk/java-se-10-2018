package tasks.task2;

@FunctionalInterface
public interface Condition<T> {
    boolean test(T t);

    // @FunctionalInterface annotation make sure that this interface hold only one abstract method
    default boolean Default(T r)
    {
        return  false ;
    }

    static void Static()
    {
    }

    private void Private()
    {
    }

}
