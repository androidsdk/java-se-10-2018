package tasks.task2;

import java.time.Year;

public class Record {
    private int ID, Year;
    private Boolean Sex;
    byte Education, Score;

    public Record(String ID, String year, String sex, String education, String score) {
        setID(ID);
        setYear(year);
        setSex(sex);
        setEducation(education);
        setScore(score);
    }

    @Override
    public String toString() {
        return "Record{" +
                "ID=" + ID +
                ", Year=" + Year +
                ", Sex=" + getSex() +
                ", Education=" + Education +
                ", Score=" + Score +
                '}';
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setID(String ID) {
        setID(Integer.valueOf(ID.trim().replaceAll("\"", "")));
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public void setYear(String year) {
        setYear(Integer.valueOf(year.trim().replaceAll("\"", "")));
    }

    public String getSex() {
        return Sex ? "Female" : "Male";
    }

    public Boolean getGenderBoolean() {
        return Sex;
    }


    public void setSex(Boolean sex) {
        Sex = sex;
    }

    public void setSex(String sex) {
        if (sex.equalsIgnoreCase("female"))
            Sex = true;
        else if (sex.equalsIgnoreCase("male"))
            Sex = false;
        else
            Sex = null;
    }

    public byte getEducation() {
        return Education;
    }

    public void setEducation(byte education) {
        Education = education;
    }

    public void setEducation(String education) {
        setEducation(Byte.valueOf(education.trim().replaceAll("\"", "")));
    }

    public byte getScore() {
        return Score;
    }

    public void setScore(byte score) {
        Score = score;
    }

    public void setScore(String score) {
        setScore(Byte.valueOf(score.trim().replaceAll("\"", "")));
    }
}
