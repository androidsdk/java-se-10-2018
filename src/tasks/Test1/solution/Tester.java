package tasks.Test1.solution;

import java.util.Locale;
import java.util.Random;

public class Tester {
    public static void main(String[] args) {
        Random random = new Random();
        AlienPack alienPack = new AlienPack(10);
        for(int i=0;i<alienPack.getAliens().length;i++)
        {
            switch (random.nextInt(3))
            {
                case 0:
                    alienPack.addAlien(new Snake(random.nextInt(101),"Snake"+i),i);
                    break;
                case 1:
                    alienPack.addAlien(new Ogre(random.nextInt(101),"Ogre"+i),i);
                    break;
                case 2:
                    alienPack.addAlien(new MarshmallowMan(random.nextInt(101),"MarshmallowMan"+i),i);
                    break;
            }

            System.out.printf(Locale.getDefault(),"%3d ) %-20s :%d %n", i+1,alienPack.getAliens()[i].name,alienPack.getAliens()[i].health );

        }
        System.out.println("alienPack.calculateDamage() = " + alienPack.calculateDamage());
        
    }
}
