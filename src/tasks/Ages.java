package tasks;

import java.util.Scanner;

public class Ages {
    public static void main(String[] args) {
        int ages = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("First age:");
        ages += scanner.nextInt();
        ages <<= 8 ;
        System.out.print("Second age:");
        ages += scanner.nextInt();
        ages <<= 8 ;
        System.out.print("Third age:");
        ages += scanner.nextInt();
        ages <<= 8 ;
        System.out.print("Fourth age:");
        ages += scanner.nextInt();

        System.out.println("ages = " + ages+ "->"+ String.format("%32s",Integer.toBinaryString(ages)).replaceAll(" ","0"));
        System.out.println("First age = "+ ((ages & 0xFF000000)>>24)+":"+ String.format("%32s",Integer.toBinaryString((ages & 0xFF000000)>>24)).replaceAll(" ","0"));
        System.out.println("Second age = "+ ((ages & 0x00FF0000)>>16)+":"+ String.format("%32s",Integer.toBinaryString((ages & 0x00FF0000)>>16)).replaceAll(" ","0"));
        System.out.println("Third age = "+ ((ages & 0x0000FF00)>>8)+":"+ String.format("%32s",Integer.toBinaryString(((ages & 0x0000FF00)>>8))).replaceAll(" ","0"));
        System.out.println("Fourth age = "+ (ages & 0x000000FF)+":"+ String.format("%32s",Integer.toBinaryString((ages & 0x000000FF))).replaceAll(" ","0"));
    }
}
