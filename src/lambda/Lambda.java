package lambda;

public class Lambda {

    public static void main(String[] args) {

        // java 8
        Task functionRef = (a, b) -> System.out.println(b + " :" + (a + 1));
        DoTask(functionRef);

        DoTask((a, b) -> System.out.println(b + " :" + (a + 2)));


        // java 7
        DoTask(new Task() {
            @Override
            public void doSomeThing(int a, String b) {
                System.out.println(b + " :" + (a + 3));
            }
        });
    }

    public static void DoTask(Task task) {
        task.doSomeThing(10, "String");
    }


    @FunctionalInterface
    static interface Task {
        void doSomeThing(int a, String b);


        default void another() {
            System.out.println("default");
        }

        static void StaticMethod() {

        }
    }
}
