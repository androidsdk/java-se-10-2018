package lambda;

public class WayOfWritinglambda {
    public static void main(String[] args) {

        method(()-> {
            System.out.println("Void");
        });
        method(()-> {
            return "Hello";
        });

        // if only one statement in the implantation then you can drop {} , ; and return statement if exist
        method(()-> System.out.println("Void"));
        method(()-> "Hello");

        method((String s)-> s.toUpperCase());
        // because in the method definition the interface SingleParameterInterface<?> accepts any type
        // then you need to specify the type in the lambda or it will be consider as Object

        // for single parameter lambda you can add or skip the ()
        method((s)-> System.out.println(s.equals("")));
        method1((s) -> s.toUpperCase());
        method1(s -> s.toUpperCase());

        // Multi parameter lambdas
        // for more than one parameters lambda you can't remove ()
        method((String s,int v)-> System.out.println(s.toUpperCase() + v));
        method((s, v)-> s.equals(v));

    }
    interface VoidMethodInterface
    {
        void voidMethod();
    }
    public static void method(VoidMethodInterface vs)
    {

    }

    interface VoidMethodWithReturnInterface
    {
        Object voidMethod();
    }
    public static void method(VoidMethodWithReturnInterface vs)
    {

    }

    interface SingleParameterInterface<T>
    {
        void single(T t);
    }
    public static void method(SingleParameterInterface<?> vs)
    {

    }

    public static void method1(SingleParameterInterface<String> vs)
    {

    }

    interface MultiParmInterface1<T,V>
    {
        void multiPram(T t, V v);
    }
    public static void method3(MultiParmInterface1<?,?> vs)
    {

    }

    interface MultiParmInterface2<T>
    {
        void multiPram(T t, int i);
    }
    public static void method(MultiParmInterface2<?> vs)
    {

    }

}
