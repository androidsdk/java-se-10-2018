package lambda;

@FunctionalInterface
public interface Condition<T> {
    //  only one abstract method
    boolean Test(T t);

    // As many methods to the function interface as default or static
    default boolean Test1()
    {
        return  false ;
    }
    static boolean Test2()
    {
        return  false ;
    }
}
