package lambda;

import tasks.task2.Condition;
import tasks.task2.Record;

import java.io.*;
import java.util.LinkedList;

public class BeforeLambda {

    public static void main(String[] args) {
        String filepath = "C:\\Users\\aalka\\IdeaProjects\\JavaSE201810\\src\\tasks\\task2\\DataSet.txt";

        LinkedList<Record> records = new LinkedList<>();
        Record temp = null ;
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filepath)))) {
            reader.readLine();
            String line = "";
            String[] split;
            while ((line = reader.readLine()) != null) {
                split = line.replace("\"","").split(",");
                temp = new Record(split[0],split[1],split[2],split[3],split[4]);
                records.add(temp);

//                if(records.size()<20)
//                    System.out.println(temp);
////                for(String s: split)
////                    System.out.printf("%20s |",s);
////                System.out.println();
            }

            //printAll(records);
            Condition<Record> condition = new Condition<Record>() {
                @Override
                public boolean test(Record record) {
                    return record.getGenderBoolean();
                }
            } ;

            Condition<Record> condition1 = new Condition<Record>() {
                @Override
                public boolean test(Record r) {
                    return ! r.getGenderBoolean() && r.getYear() == 2004;
                }
            } ;
//            printWithCondition(records, condition1);
            printWithCondition(records, new Condition<Record>() {
                @Override
                public boolean test(Record r) {
                    return r.getEducation()==2;
                }
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printWithCondition(LinkedList<Record> records, Condition<Record> condition) {
        for(Record r : records)
            if(condition.test(r))
                System.out.println(r);
    }

    private static void printAll(LinkedList<Record> records) {
        for(Record r : records)
            System.out.println(r);
    }
}
