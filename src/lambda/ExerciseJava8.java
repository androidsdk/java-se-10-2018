package lambda;

import lambda.extras.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;


public class ExerciseJava8 {

	public static void main(String[] args) {
		// 1: Create list of person in the class
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Ahmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)

		);

		// 2: Sort list by last name

//		// Java 7
//		Collections.sort(people, new Comparator<Person>() {
//			@Override
//			public int compare(Person o1, Person o2) {
//				return o1.getName().compareTo(o2.getName());
//			}
//		});

		// Java 8

		Collections.sort(people, (p1, p2) -> p1.getName().compareTo(p2.getName()));

		// 3: Create a method that prints all elements in the list

		System.out.println("=======Sorted by name =======");

//		// Java 7
//		printAll(people);

		// Java 8
		printConditionally(people,(p) -> true );

//		Collections.sort(people, new Comparator<Person>() {
//			@Override
//			public int compare(Person o1, Person o2) {
//				return o1.getAge().compareTo(o2.getAge());
//			}
//		});
//		System.out.println("=======Sorted by age =======");
//		printAll(people);

		Collections.sort(people, (p1, p2) -> p1.getAge().compareTo(p2.getAge()));
		System.out.println("=======Sorted by age =======");
		printConditionally(people,(p) -> true );


		System.out.println("\nPrinting all persons in the class");
//		printAll(people);
		printConditionally(people,(p) -> true );
		// 4: Create a method that prints all people that have last name beginning with M

//		// Java 7
//		System.out.println("\nPrinting all persons with last name beginning with M");
//		printConditionally(people, new Predicate<Person>() {
//			@Override
//			public boolean test(Person p) {
//				return p.getName().toUpperCase().contains(", M");
//			}
//		});


		// Java 8

		System.out.println("Printing all persons with last name beginning with M");
		printConditionally(people, p -> p.getName().toUpperCase().contains(", M"));

		System.out.println("Printing all persons with first name beginning with A");
		printConditionally(people, p -> p.getName().toUpperCase().startsWith("A"));
	}

	private static void printConditionally(List<Person> people, Condition condition) {
		for (Person p : people) {
			if (condition.test(p)) {
				System.out.println(p);
			}
		}

	}


	interface Condition {
		boolean test(Person p);
	}

}
