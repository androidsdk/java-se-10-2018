package lambda;

import lambda.extras.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;

public class StandardFunctionalInterfacesDemo {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Ahmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)
				);
		
		//  2: Sort list by last name
		Collections.sort(people, (p1, p2) -> p1.getName().compareTo(p2.getName()));
//		Comparator<Person> personComparator = new Comparator<Person>() {
//			@Override
//			public int compare(Person o1, Person o2) {
//				return 0;
//			}
//		}
		// rewrite using lambda expression
		people.sort((p1,p2) ->  p1.getName().compareTo(p2.getName()));

		// rewrite using method referencing
		people.sort(StandardFunctionalInterfacesDemo::comparePerson);


		int[] ages = people.stream().mapToInt(p->p.getAge()).toArray();
		
		// 3: Create a method that prints all elements in the list
		System.out.println("Printing all persons");
		performConditionally(people, p -> true, p -> System.out.println(p));
		
		// 4: Create a method that prints all people that have last name beginning with M
		System.out.println("Printing all persons with last name beginning with M");
		performConditionally(people, p -> p.getName().startsWith("M"), p -> System.out.println(p));

		System.out.println("Printing all persons with first name beginning with M");
		
		performConditionally(people, p -> p.getName().startsWith("M"), p -> System.out.println(p.getName()));

	}

	private static int comparePerson(Person p1, Person p2)
	{
		return  p1.getName().compareTo(p2.getName() );
	}
	private static  void DoAction(List<Person> people, Consumer<Person> consumer)
	{

		// External looping
//		for (Person p : people) {
//				consumer.accept(p);
//		}

		// or you can write as internal looping
		people.forEach(consumer);


	}
	private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
		for (Person p : people) {
			if (predicate.test(p)) {
				consumer.accept(p);
			}
		}
	}
}
