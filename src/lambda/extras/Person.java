package lambda.extras;

import java.util.Locale;

public class Person implements Cloneable {

	private int ID ;
	private Integer Age, PClass;
	private String Name;
	private Boolean Gender, Survived;

	public Person(String ID, String name, String pclass, String age, String gender, String survived) {
		setID(ID);
		setName(name);
		setPClass(pclass);
		setAge(age);
		setGender(gender);
		setSurvived(survived);
	}

	public Person(String first, String last, int age) {
		setAge(String.valueOf(age));
		setName(first+", "+last);

	}


	@Override
	public String toString() {
		return String.format(Locale.getDefault(),"Person{ID:%5d, Name:%-65s, PClass:%-5s, Age:%4d, Gender:%-8s,Survived:%-4s}",getID(),getName(),getPClass(),getAge(),getGender(),getSurvived() );
	}

	public int getID() {
		return ID;
	}



	public void setID(int ID) throws Exception {
		if(ID >= 0 )
			this.ID = ID;
		else
			throw new Exception("Negative ID");
	}
	public void setID(String ID) {
		try {
			setID(Integer.valueOf(ID));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getPClass() {
		if(PClass == null)
			return "*";
		switch (PClass)
		{
			case 1: return "1st";
			case 2: return "2nd";
			case 3: return "3rd";

			default:
				return PClass+"th";

		}

	}

	public void setPClass(int PClass) {
		this.PClass = PClass;
	}
	public void setPClass(String PClass) {
		if(PClass.length()>=3)
			this.PClass = Integer.valueOf(PClass.substring(0,PClass.length()-2));
		else
			PClass = null ;
	}

	public Integer getAge() {
		return Age;
	}

	public void setAge(Integer age) {
		if(age >= 0)
			Age = age;
		else
			throw new IllegalArgumentException("Invalid age value");
	}
	public void setAge(String age) {
		if(age.equalsIgnoreCase("na"))
			Age = null;
		else
		{
			try {
				setAge(Integer.valueOf(age));
			}catch (Exception e)
			{
				Age = 0 ;
			}
		}

	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getGender() {
		return Gender == null ? "NA" : Gender ? "Female" : "Male";
	}

	public void setGender(Boolean gender) {
		Gender = gender;
	}
	public void setGender(String gender) {
		if(gender.equalsIgnoreCase("female"))
			Gender = true;
		else if(gender.equalsIgnoreCase("male"))
			Gender = false ;
		else
			Gender = null ;
	}

	public String getSurvived() {
		if(Survived == null)
			return "NA";
		return Survived ? "Yes":"No";
	}

	public void setSurvived(boolean survived) {
		Survived = survived;
	}

	public void setSurvived(String survived) {
		if(survived.trim().equalsIgnoreCase("1"))
			Survived = true;
		else if(survived.trim().equalsIgnoreCase("0"))
			Survived = false;
		else
			Survived = null ;
	}
	
}
