package lambda.extras;

import lambda.Action;
import lambda.Condition;

public class Lambda3 {

    public static void main(String[] args) {

        // Call using option 1: anonymous class
        method(new Condition<String>() {
            @Override
            public boolean Test(String s) {
                return false;
            }
        });

        // Call using option 2: Using lambda
        method(s ->  false );
        method((s) ->  false );
        method((String s) ->  false );
        method(s -> { return false ; } );
        method((s) ->  { return  false ; } );


        // Using closures



        method(s->true,s -> System.out.println(s));         // Or
        method(s->true,System.out::println);




    }

    public static void method(Condition<String> condition)
    {

    }

    public static void method(Condition<String> condition, Action<String> action)
    {

    }
}
