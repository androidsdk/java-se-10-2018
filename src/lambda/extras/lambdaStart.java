package lambda.extras;


/*
#includ.....

void main()
{
}
 */
public class lambdaStart {

    interface  MyInterface {
        void todo(int a);
    }

    public static void main(String[] args) {


        Runnable fun =   ()-> {
                //TODO :
            }
        ;
        Thread t = new Thread(() -> {
            System.out.println();
        });

        foo(new MyInterface() {
            @Override
            public void todo(int a) {

            }
        });

        foo((a) -> {
            System.out.println(a);
        });


    }

    public static void foo(MyInterface myInterface)
    {
        for(int i = 0; i< 10;i++)
        {
            myInterface.todo(i);
        }
    }
}
