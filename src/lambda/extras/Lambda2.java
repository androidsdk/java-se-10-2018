package lambda.extras;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Lambda2 {

    public static void main(String[] args) {

        ArrayList<Person> personArrayList = new ArrayList<>();
        String line = "";
        String[] slitted;

        try (BufferedReader reader = new BufferedReader(new FileReader(new File("test.txt")))) {
            line = reader.readLine();
            while ((line = reader.readLine()) != null) {
                slitted = line.replaceAll("\"", "").split("(,(?=[\\d\\S]))");

                Person p = new Person(slitted[0], slitted[1], slitted[2], slitted[3], slitted[4], slitted[5]);
                personArrayList.add(p);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // TODO : printArray

    // TODO : printArrayConditionally

    // TODO : performActionConditionally



}
