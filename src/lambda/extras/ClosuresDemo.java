package lambda.extras;

public class ClosuresDemo {

	public static void main(String[] args) {
		int a = 10;
		final int b = 0;
		doProcess(a, i ->
		{
			try {
				System.out.println(i / b);
			}
			catch (Exception e)
			{

			}
		});

//		doProcess(a,new Process(){
//
//			@Override
//			public void process(int i) {
//				System.out.println(i / b) ;
//
//			}
//		});
		 //b= 10;
	}
	
	public static void doProcess(int i, Process p) {
		p.process(i);
	}

}

interface Process {
	void process(int i);
}
