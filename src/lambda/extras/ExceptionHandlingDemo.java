package lambda.extras;

import java.util.function.BiConsumer;

public class ExceptionHandlingDemo {

	public static void main(String[] args) {
		int [] someNumbers = { 1, 2, 3, 4 };
		int key = 0;
		long start = 0, end = 0 ;


		process(someNumbers,key,wrapperLambda((v,k)-> System.out.println(v/k)));


//
//		start = System.nanoTime();
//		process(someNumbers, key, wrapperLambda((v, k) -> System.out.println(v / k)));
//		end = System.nanoTime() - start ;
//		System.out.println("Time warpper :"+end);

//		start = System.nanoTime();
//		process(someNumbers, key, (v, k) -> System.out.println(v / k));
//		end = System.nanoTime() - start ;
//		System.out.println("Time  :"+end);


	}

	private static void process(int[] someNumbers, int key, BiConsumer<Integer, Integer> consumer) {

		for (int i : someNumbers) {
			consumer.accept(i, key);
		}
	}

	private static BiConsumer<Integer, Integer> wrapperLambda(BiConsumer<Integer, Integer> consumer) {
		return (v, k) ->  {
			try {
				consumer.accept(v, k);
			}
			catch (ArithmeticException e) {
				System.out.println("Exception caught in wrapper lambda");
			}
			
		};
	}
	

}
