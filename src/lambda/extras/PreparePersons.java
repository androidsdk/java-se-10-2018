package lambda.extras;

import lambda.Action;
import lambda.Condition;

import java.io.*;
import java.util.ArrayList;

public class PreparePersons {

    public static void main(String[] args) {
        ArrayList<Person> personArrayList = new ArrayList<>();
        String line = "";
        String [] slitted ;

        try(BufferedReader reader = new BufferedReader(new FileReader(new File("test.txt"))))
        {
            line = reader.readLine();
            while(( line = reader.readLine()) != null)
            {
                slitted = line.replaceAll("\"","").split("(,(?=[\\d\\S]))");

                Person p = new Person(slitted[0],slitted[1],slitted[2],slitted[3],slitted[4],slitted[5]);
                personArrayList.add(p);

            }

            // 1313 , 750  , 50
            //personArrayList.stream().forEach(System.out::println);
            long counter = personArrayList.stream()
                    .filter(p->p.getAge() == null)
                    .mapToInt(p->1)
                    .sum();
            System.out.println(counter);

//                    .filter(p->  p.getAge()!=null && p.getAge()>50)
//                    .forEach(System.out::println);
            //Print(personArrayList);
//            PrintConditionally(personArrayList, new Condition<Person>() {
//                @Override
//                public boolean Test(Person o) {
//                    return o.getSurvived().equalsIgnoreCase("yes") && o.getGender().equalsIgnoreCase("male");
//                }
//            });

          //  PrintConditionally(personArrayList, (p) -> p.getSurvived().equalsIgnoreCase("yes") && p.getGender().equalsIgnoreCase("male"));

            // Print all records with age == null
            DoActionConditionally(personArrayList,p->p.getAge()==null,p-> System.out.println(p));

            // With method referencing
            DoActionConditionally(personArrayList,p->p.getAge()==null,System.out::println);
            // Print the age of all records with age != null
            DoActionConditionally(personArrayList,p->p.getAge()!=null,p-> System.out.println(p.getAge()));

            DoActionConditionally(personArrayList,p->p.getName().contains("Mrs ")&& p.getGender().equalsIgnoreCase("male"),p-> System.out.println(p));



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void Print(ArrayList<Person> persons)
    {
        int counter = 0 ;
        for(Person p : persons) {
            System.out.println(p);
            counter++;

        }

        String isAre = counter> 1 ?"are" : "is" ;
        System.out.printf("There %s %s person printed.%n",isAre,counter);
    }
    private static void PrintConditionally(ArrayList<Person> persons, Condition<Person> condition)
    {
        int counter = 0 ;
        for(Person p : persons) {               // data
            if (condition.Test(p)) {            // condition
                System.out.print(p);            // action
                counter++;
            }
        }
        String isAre = counter> 1 ?"are" : "is" ;
        System.out.printf("There %s %s person printed.%n",isAre,counter);
    }

    private static void DoActionConditionally(ArrayList<Person> persons, Condition<Person> condition, Action<Person> action)
    {
        int counter = 0 ;
        for(Person p : persons) {
            if (condition.Test(p)) {
                action.Perform(p);
                counter++;
            }
        }
        String isAre = counter> 1 ?"are" : "is" ;
        System.out.printf("There %s %s person printed.%n",isAre,counter);
    }
}
