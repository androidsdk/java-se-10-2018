package lambda;

import lambda.extras.Person;

import java.util.*;
import java.util.function.Predicate;


public class Exercise {

	public static void main(String[] args) {
		// 1: Create list of person in the class
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Ahmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)

		);

		// 2: Sort list by last name

		Collections.sort(people, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		System.out.println("=======Sorted by name =======");
		printAll(people);



		Collections.sort(people, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getAge().compareTo(o2.getAge());
			}
		});
		System.out.println("=======Sorted by age =======");
		printAll(people);
		// 3: Create a method that prints all elements in the list

		System.out.println("\nPrinting all persons in the class");
		printAll(people);


		// 4: Create a method that prints all people that have last name beginning with M

		System.out.println("\nPrinting all persons with last name beginning with M");
		printConditionally(people, new Predicate<Person>() {
			@Override
			public boolean test(Person p) {
				return p.getName().toUpperCase().contains(", M");
			}
		});
	}

	private static void printAll(List<Person> people) {
		for (Person p : people) {
			System.out.println(p);
		}

	}
	private static void printConditionally(List<Person> people, Predicate<Person> predicate) {
		for (Person p : people) {
			if (predicate.test(p)) {
				System.out.println(p);
			}

		}

	}




}
