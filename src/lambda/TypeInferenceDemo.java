package lambda;

public class TypeInferenceDemo {

	public static void main(String[] args) {
		// how the compile knows the type of the lambda variable s in the following  example?
		// By type Inferences the information from the interface itself <StringLengthLambda>
		// this interface must be functional interface so lambda expression can be used
		printLambda(s -> s.length());
		
	}
	
	
	public static void printLambda(StringLengthLambda l) {
		System.out.print(l.getLength("Hello Lambda"));
	}
	
	
	interface StringLengthLambda {
		int getLength(String s);
	}
	
	

}
