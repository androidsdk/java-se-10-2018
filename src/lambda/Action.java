package lambda;

public interface Action<T> {
    void Perform(T t);
}

