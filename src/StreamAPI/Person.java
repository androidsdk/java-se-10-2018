package StreamAPI;

public class Person implements Cloneable {

	public enum  Gender {MALE ,FEMALE};
	private String firstName;
	private String lastName;
	private int age;
	private Gender gender ;

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Person(String firstName, String lastName,Gender gen, int age) {
		super();
		setFirstName(firstName);
		setLastName(lastName);
		setAge(age);
		setGender(gen);
	}


	@Override
	protected Person clone() throws CloneNotSupportedException {
		return new Person(getFirstName(),getLastName(),getGender(),getAge());
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
	}
	
	
	
}
