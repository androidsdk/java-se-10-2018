package StreamAPI;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamsDemo {

    public static void iterate(List<Person> list, Predicate<Person> op1, Predicate<Person> op2, Consumer<Person> consumer) {
        for (Person p : list) {
            if (op1.test(p) && op2.test(p))
                consumer.accept(p);
        }
    }

    public static void main(String[] args) {

        Random r = new Random();
        List<Person> people = new LinkedList<>();
        people.add(new Person("Ahmed", "MAlkaff", Person.Gender.MALE, 34));
        people.add(new Person("Omama", " Mannaa", Person.Gender.FEMALE, 27));
        people.add(new Person("Raghad", "Mando", Person.Gender.FEMALE, 20));
        people.add(new Person("Yazan", "AbuHamdodeh", Person.Gender.MALE, 26));
        people.add(new Person("Imaseil", "Bibers", Person.Gender.MALE, 23));
        people.add(new Person("Ahmed", "Dosogqei", Person.Gender.MALE, 25));
        people.add(new Person("Yaser", "Meryan", Person.Gender.MALE, 23));


        long count = people.stream().count();
        System.out.println("count = " + count);

//        people.stream().forEach(System.out::println);

        people.stream()
                .filter(p -> p.getGender() == Person.Gender.MALE)
                .map(p -> p.getAge())
              //  .filter(p -> p.startsWith("A"))
                .forEach(System.out::println);
        
        int sum = people.stream()
                .filter(p->p.getGender() == Person.Gender.MALE)
                .mapToInt(p->p.getAge())
                .sum();

        System.out.println("sum = " + sum);

        int max = people.stream().max((p,p1)->Integer.compare(p.getAge(), p1.getAge())).get().getAge();
        System.out.println("max = " + max);

        max = people.stream().mapToInt(p->p.getAge()).max().getAsInt();
        System.out.println("max = " + max);
        people.stream()
                .map(person -> person.getAge())
                 .filter(age -> age >= 25)
                .forEach(System.out::println);

        int result =people.stream()
                .filter(p->p.getGender() == Person.Gender.FEMALE)
                .mapToInt(person -> person.getAge())
                .filter(age -> age >= 25)
                .sum();

        System.out.println("result = " + result);

        List<String> names = new ArrayList<>();
                people.stream()
                .filter(p->p.getGender()== Person.Gender.MALE)
                .map(p->p.getFirstName())
                .forEach(p->names.add(p));

        System.out.println("names = " + names);

        List<String> firstNames = people.stream()
                .filter(p->p.getGender()== Person.Gender.MALE)
                .map(p->p.getFirstName())
                //.collect(Collectors.toList());
                .collect(ArrayList<String>::new,(list, element)-> list.add("\'"+element+"\'"),(list1, list2) -> list1.addAll(list2));
//
        System.out.println("firstNames = " + firstNames);


//        Thread t = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println();
//            }
//        });
//        for (int i = 0; i < 1000; i++) {
//            Person p = null;
//
//            try {
//                p = people.get(0).clone();
//                //System.out.println((char)('a' + r.nextInt('Z'-'A'+1)));
//
//                p.setFirstName((char) ('A' + r.nextInt(27)) + p.getFirstName());
//                p.setLastName((char) ('A' + r.nextInt(27)) + p.getLastName());
//
//                people.add(p);
//            } catch (CloneNotSupportedException e) {
//                e.printStackTrace();
//            }
//
//        }

//        long start1 = System.nanoTime();
//
//        for (int i = 0; i < people.size(); i++) {
//            System.err.println(Thread.currentThread().getName()+ ": " + people.get(i));
//        }
//        long end1 = System.nanoTime()-start1;
//
//        long start2 = System.nanoTime();
//        for (Person p : people) {
//            System.err.println(Thread.currentThread().getName()+ ": " +p);
//        }
//
//        long end2 = System.nanoTime()-start2;

//
//        long start3 = System.nanoTime();
//        people.parallelStream().forEach((p) -> System.err.println(Thread.currentThread().getName() + ": " + p));
//        long end3 = System.nanoTime() - start3;

//        long start4 = System.nanoTime();
//        people.stream().forEach(System.err::println);
//        long end4 = System.nanoTime()-start4;


//        System.out.println("for loop time     :"+end1);
//        System.out.println("foreach time      :"+end2);
        // System.out.println("stream  time      :"+end4);
//        System.out.println("stream time      :" + end3);


//        iterate(people,p -> p.getLastName().startsWith("M"),p->p.getFirstName().contains("a"),p -> System.out.println(p));
//
//        System.out.println("-----------------------------------------------------");
//        people.stream()
//                .filter(p -> p.getLastName().startsWith("M"))
//                //.filter(p -> p.getFirstName().contains("a"))
//                .forEach(p -> System.out.println(Thread.currentThread().getName()+ ": " +p));
//
//        System.out.println("stream time      :"+end3);
////
//		for(int i=0;i<100;i++)
//		{
//			Person p = null;
//
//			try {
//				p = people.get(0).clone();
//				//System.out.println((char)('a' + r.nextInt('Z'-'A'+1)));
//
//				p.setFirstName((char)('A' + r.nextInt(27))+ p.getFirstName());
//				p.setLastName((char)('A' + r.nextInt(27))+ p.getLastName());
//
//				people.add(p);
//			} catch (CloneNotSupportedException e) {
//				e.printStackTrace();
//			}
//
//		}

//		long start = System.nanoTime();
//
//		people.parallelStream()
//				.filter(p -> p.getLastName().startsWith("M"))
//				.forEach(p -> System.out.println(Thread.currentThread().getName()+":"+p.getFirstName()));
//
//		System.out.println("Ends on :"+(System.nanoTime()-start));
//
//		long start1 = System.nanoTime();
//		people.stream()
//				.filter(p -> p.getLastName().startsWith("M"))
//				.forEach(p -> System.out.println(Thread.currentThread().getName()+":"+p.getFirstName()));
//
//		System.out.println("Ends on :"+(System.nanoTime()-start));
//
//
//		long count = people.parallelStream()
//		.filter(p -> p.getLastName().startsWith("D"))
//		.count();
//
        //System.out.println(count);


    }

}
