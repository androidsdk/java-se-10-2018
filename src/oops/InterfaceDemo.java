package oops;

import java.util.Locale;
import java.util.Random;

public class InterfaceDemo {

       interface Flyable{
           // this is an abstract public method
           // can't be private
          default  String canFly ()
         {
             return ", it can fly!";
         }

        static void staticMethod()
        {
            // Interfaces can have static methods with a body
            // by default : static method are public and it can be private
            // static method has nothing to do with inheritance
        }

        private  void privateMethod()
        {
            // interface can have a private method with a body
        }

        default void defaultMethod(){
            // interface can have a default method with a body

            // private methods are usable only from the default methods
            privateMethod() ;

        }
    }
    static class Animal {
        @Override
        public String toString() {
            return "Animal{}";
        }
    }

    static class Mammal extends Animal {
        @Override
        public String toString() {
            return "Mammal{}";
        }
    }

    static class Bird extends Animal {
        @Override
        public String toString() {
            return "Bird{}";
        }
    }

    static class Penguin extends Bird {
        @Override
        public String toString() {
            return "Penguin{}";
        }
    }

    static class Falcon extends Bird  implements Flyable{
        @Override
        public String toString() {
            return "Falcon{}";
        }
//        @Override
//        public String canFly() {
//            return ", it can fly!";
//        }
    }

    static class Bat extends Mammal implements Flyable{
        @Override
        public String toString() {
            return "Bat{}";
        }

//        @Override
//        public String canFly() {
//            return ", it can fly!";
//        }

    }

    static class Cat extends Mammal {
        @Override
        public String toString() {
            return "Cat{}";
        }
    }


    public static void main(String[] args) {
        Random random = new Random();
        Animal[] animals = new Animal[20];
        for (int i = 0; i < animals.length; i++) {
            switch (random.nextInt(7)) {
                case 0:
                    animals[i] = new Cat();
                    break;
                case 1:
                    animals[i] = new Bat();
                    break;
                case 2:
                    animals[i] = new Falcon();
                    break;
                case 3:
                    animals[i] = new Penguin();
                    break;
                case 4:
                    animals[i] = new Mammal();
                    break;
                case 5:
                    animals[i] = new Bird();
                    break;
                default:
                    animals[i] = new Animal();
                    break;
            }

            System.out.printf(Locale.getDefault(), "%3d) %s", i + 1, animals[i]);

//            if (animals[i] instanceof Bat  ) {
//                System.out.println(((Bat) animals[i]).canFly());
//            } else if (animals[i] instanceof Falcon) {
//                Falcon f = (Falcon) animals[i];
//                System.out.println(f.canFly());
//            } else
//                System.out.println();

            if(animals[i] instanceof  Flyable)
                System.out.println(((Flyable) animals[i]).canFly());
            else
                System.out.println();
        }


        Falcon f = new Falcon();
        f.defaultMethod();


        Flyable.staticMethod();         // call the static method of the interface

    }


}
