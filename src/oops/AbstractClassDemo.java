package oops;

public class AbstractClassDemo {

    static abstract class ParentAbstract
    {
        public ParentAbstract() {
            System.out.println("ParentAbstract()");
        }
        public abstract  void AbstractMethod();
        public void NonAbstractMethod()
        {
        }
    }

    static public class ChildOfAbstract extends  ParentAbstract {

        public ChildOfAbstract() {
            System.out.println("ChildOfAbstract()");
        }

        @Override
        public void AbstractMethod() {
            System.out.println("ChildOfAbstract:AbstractMethod()");
        }
    }

    public static void main(String[] args) {

        //ParentAbstract anAbstract = new ParentAbstract();
        //anAbstract.AbstractMethod();

        ParentAbstract ps = new ChildOfAbstract();

        System.out.println("--------------------");
        ParentAbstract ps1 = new ParentAbstract() {
            @Override
            public void AbstractMethod() {
                System.out.println("AnonymousInnerType:AbstractMethod()");
            }
        };

        ps.AbstractMethod();
        ps1.AbstractMethod();

    }
}
