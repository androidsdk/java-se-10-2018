package oops;

import shared.Employee;
import shared.Person;
import shared.Student;

public class InheritanceAndPlymorphismDemo {

    public static void main(String[] args) {

        Person p = new Person();
        Person ps = new Student();
        Person pe = new Employee();
        Student s = new Student();
        Employee e = new Employee();

        s.Major = "" ;


        p.Name = "p";
        ps.Name = "ps" ;
        pe.Name = "pe";
        s.Name = "s";
        e.Name = "e";

        System.out.println("p = " + p);
        System.out.println("ps = " + ps);
        System.out.println("pe = " + pe);
        System.out.println("s = " + s);
        System.out.println("e = " + e);

        p.DoSome();
        ps.DoSome();
        pe.DoSome();
        s.DoSome();
        e.DoSome();

        // Variable type determines what what methods you can see and what variable you can use
        // Object type determines what implementation of method you will call/ use
        ((Person) e).Name = "123456";
        e.Name = "88554";
        System.out.println(((Person) e).Name);
        System.out.println( e.Name);


    }
}
