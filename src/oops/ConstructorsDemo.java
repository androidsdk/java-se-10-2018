package oops;

public class ConstructorsDemo {


    // Constructor : is a special method , must hold the same class name with no return data type
    // call after create an object
    // can be overloaded
    // can't be inherited or overridden
    // if the developers did not create one, then the compiler will create an empty  constructor called the default constructor
    // ** The first statement of any constructor body is call to another constructor
    // ** if the developer did not call another constructor in the first statement of the constructor
    // ** then the compiler will call super();

    // The call of another constructor could be in one of the following options
    // 1) call constructor from the parent class : super([parameter list])
    // 2) call constructor from the same class   : this([parameter list])

    static class MyClass {

        public  MyClass()
        {
            //this(0);
//            System.out.println("MyClass()");
        }
        public  MyClass(int a)
        {
            this();
//            System.out.println("MyClass(int a)");
        }
    }

    static  class  SubClass extends MyClass {

        public SubClass()
        {
            this(0);
//            System.out.println("SubClass()");
        }

        public  SubClass (int a)
        {
            super(a);
//            System.out.println("SubClass(int)");
        }

        @Override
        protected void finalize() throws Throwable {
            System.out.println("I will be destroyed");
            super.finalize();
        }
    }


    public  static  void method()
    {
        MyClass a = new MyClass();
        System.out.println("----------");
        SubClass s = new SubClass();
        System.out.println("----------");
        SubClass s1 = new SubClass(10);

        for(int i=0;i<10000000;i++)
        {
            s = new SubClass();
        }
    }
    public static void main(String[] args) {
        method();
       // System.gc();

    }
}
