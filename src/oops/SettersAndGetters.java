package oops;

import shared.User;

public class SettersAndGetters {

    public static void main(String[] args) {
        User user = new User();
        //user.id = 1;

        try {
            user.setAge(350);
        }catch (IllegalArgumentException ex)
        {

        }

        //user.age = 350 ;
        //user.Name = "07752441";
        user.setName("075241");
        //user.Login = "a.alkaff@lkjdf";
        //user.Password = "123";
        user.changePassword("jnew");
        int age = user.getAge() ;
        System.out.println("user = " + user);
    }
}
