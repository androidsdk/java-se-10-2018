package oops;

public class AbstractClassVsInterface {
    interface  IInterface {

        // All variables in interface are public static final
        int x = 20;
        // public  abstract  void AbstractMethod();
        void AbstractMethod();
        default  void AbstractMethod1(){}

        //
        int A  = 10;

        // Three cases can use a body with a method inside an interface
        // static methods (java 8 and above), default methods (java 8 and above) and private methods (java 9 and above)
        default void DefaultMethod()
        {
            PrivateMethod();
        }
        private void PrivateMethod()
        {
            // this is a private method , starting from Java 9
        }

        static void  StaticMethod()
        {

        }
    }

    static  class  A implements IInterface {

        @Override
        public void AbstractMethod() {

        }

        @Override
        public void DefaultMethod() {

        }
    }
    interface  IInterface2 extends  IInterface {

    }

    static  class  Child implements  IInterface {

        @Override
        public void AbstractMethod() {

        }
    }
    static abstract class TheClass implements  IInterface2
    {
         int A = 10 ;

        public static void main(String[] args) {

            // here no object of type IInterface is create ,
            // it will create an object of an anonymous inner type
            IInterface iInterface = new IInterface() {
                @Override
                public void AbstractMethod() {
                }
            };

            // here an object of type Child is created , but no object of IInterface
            IInterface iInterface1 = new Child();


        }
    }
}
