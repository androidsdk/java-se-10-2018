package oops;

import shared.EncapsulationClass;

public class ChildInOtherPackage extends  EncapsulationClass {

    public void method()
    {
        Protected = 10 ;  // object can access the protected data member of their
                        // parent object only
        EncapsulationClass aClass = new EncapsulationClass();

    }
    public static void main(String[] args) {
        EncapsulationClass aClass = new EncapsulationClass();
        aClass.Public = 10 ;    // because its public for all
        //aClass.Package = 10 ;   // in the same package
        //aClass.Protected = 10 ;  // because protected is also package
    }
}
