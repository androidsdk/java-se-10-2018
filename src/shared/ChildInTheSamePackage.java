package shared;

public class ChildInTheSamePackage extends  EncapsulationClass {

    public static void main(String[] args) {
        EncapsulationClass aClass = new EncapsulationClass();
        aClass.Public = 10 ;    // because its public for all
        aClass.Package = 10 ;   // in the same package
        aClass.Protected = 10 ;  // because protected is also package
    }
}
