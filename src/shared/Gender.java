package shared;

public enum Gender {
    Male,
    Female,
    Trans
}
