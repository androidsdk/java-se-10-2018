package shared;

public class Point implements Cloneable{
    public  int x , y ,z ;

    @Override
    public Point clone() {
        Point temp = new Point() ;
        temp.x = x ;
        temp.y = y ;
        temp.z = z ;
        return temp;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Point)
        {
            Point t = (Point) obj;
            return  x == t.x && y == t.y && z == t.z ;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Point[" + x +
                "," + y +
                "," + z +
                ']';
    }
}
