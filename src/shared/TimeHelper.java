package shared;

import java.util.Locale;

public class TimeHelper {
    public static String timeToString(long time) {
        if (time < 1000) return String.format(Locale.getDefault(), "%8d (ns)", time);
        if (time < 1000000) return String.format(Locale.getDefault(), "%8.2f (µs)", (time / 1000.0));
        if (time < 10000000000L) return String.format(Locale.getDefault(), "%8.2f (ms)", (time / 1000000.0));
        return String.format(Locale.getDefault(), "%8.2f (s)", (time / 1000000000.0));
    }
}
