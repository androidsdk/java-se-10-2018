package FilesIO;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

public class FileIO {

    public static void main(String[] args) {

        String filename = "Test.txt";
//        long m1 = readTextFileMethod1("Test1.txt");
//        long m2 = readTextFileMethod2("Test.txt");
//        long m3 = readTextFileMethod3("Test2.txt");
        long m4 = readingBinaryFileMethod1("I:\\Downloads\\Git-2.19.2-64-bit.exe");
        System.out.printf(Locale.getDefault(), "Reading Binary file  time     :%15d %n", m4);

        long m5 = copyBinaryFileMethod1("I:\\Downloads\\Git-2.19.2-64-bit.exe", "target.exe");
        System.out.printf(Locale.getDefault(), "Copying Binary file  time     :%15d %n", m5);

//        System.out.printf(Locale.getDefault(),"Reading file using Scanner time     :%15d %n" , m1);
//        System.out.printf(Locale.getDefault(),"Reading file using BufferReader time:%15d %n" , m2);
//        System.out.printf(Locale.getDefault(),"Reading file using File API time    :%15d %n" , m3);
//
//        System.out.println("m1/m2 = " + m1/(double)m2);
//        System.out.println("m2/m1 = " + m2/(double)m1);
    }


    public static long readTextFileMethod1(String filepath) {

        int lines = 1;
        // using the scanner
        long start = System.nanoTime();
        try (Scanner readFromFile = new Scanner(new File(filepath))) {

            while (readFromFile.hasNextLine()) {

                readFromFile.nextLine();
                //System.out.printf(Locale.getDefault(),"%5d ->%s%n",lines++, readFromFile.nextLine());
            }
            System.out.println("lines = " + lines);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return System.nanoTime() - start;

    }

    public static long readTextFileMethod2(String filepath) {

        // using the scanner
        int lines = 1;
        String line = "";
        long start = System.nanoTime();
        try (BufferedReader reader = new BufferedReader(new FileReader(filepath))) {

            while ((line = reader.readLine()) != null) {
                //System.out.printf(Locale.getDefault(),"%5d ->%s%n",lines++, line);
            }
            System.out.println("lines = " + lines);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return System.nanoTime() - start;

    }

    public static long readTextFileMethod3(String filepath) {

        // using the scanner
        int lines = 1;

        long start = System.nanoTime();
        try {
            // Since Java 7
            List<String> linesArrayLists = Files.readAllLines(Paths.get(filepath), Charset.forName("UTF-8"));
            for (String line : linesArrayLists) {
                lines++;
            }
            System.out.println("lines = " + lines);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return System.nanoTime() - start;

    }

    public static long readingBinaryFileMethod1(String filepath) {
        int readBytes, size = 0;
        long start = System.nanoTime();
        try (FileInputStream fis = new FileInputStream(filepath)) {
            byte[] buffer = new byte[1024];
            while ((readBytes = fis.read(buffer)) != -1) {

                System.out.println("readBytes = " + readBytes);
                size += readBytes;
            }

            System.out.println("size = " + size);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return System.nanoTime() - start;
    }

    public static long copyBinaryFileMethod1(String filepath, String target) {
        int readBytes, size = 0;
        long start = System.nanoTime();
        try (FileInputStream fis = new FileInputStream(filepath);
             FileOutputStream fos = new FileOutputStream(target)) {
            byte[] buffer = new byte[1024];
            while ((readBytes = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, readBytes);
            }

            System.out.println("size = " + size);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return System.nanoTime() - start;
    }

    // java 7 or above
    public static long copyBinaryFileMethod2(String filepath, String target) {
        long start = System.nanoTime();
        try{
            Files.copy(Paths.get(filepath),Paths.get(target), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return System.nanoTime() - start;
    }


}
