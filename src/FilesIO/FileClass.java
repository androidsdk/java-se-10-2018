package FilesIO;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Locale;

public class FileClass {
    public static void main(String[] args) {
        File file = new File("I:\\Downloads");

        if(file.exists())
        {
            System.out.println("file.getName() = " + file.getName());
            System.out.println("file.getParent() = " + file.getParent());
            System.out.println("file.getAbsolutePath() = " + file.getAbsolutePath());
            System.out.println("file.getPath() = " + file.getPath());
            System.out.println("file.length() = " + file.length());

            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".pdf");
                }
            };
            File[] files = file.listFiles(filenameFilter);
            for(File f: files)
                System.out.println(f.getName());
        }
        else
            System.err.printf(Locale.getDefault(),"File %s is not found!",file.getAbsolutePath());
    }


}
