package regex;



import java.io.*;
import java.sql.Connection;
import java.util.Collections;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Finder {

    public static void main(String[] args) {

        int lineNumber = 1 ;

        String regx = "(\\w+)@(\\w+).(\\w+)";
        Pattern pattern = Pattern.compile(regx);

        Predicate<String> filter = s -> pattern.matcher(s).find() ;

        //Predicate<String> matcher = s -> pattern.matcher(s).find();

        Consumer<Matcher> sub = s -> System.out.println(s.group());
        Function<String,String> mapper = input ->    pattern.matcher(input).group() ;

        //Matcher matcher = pattern.
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("D:\\bad emals.txt")))
        {
            bufferedReader.lines().filter(filter).collect(Collectors.<String>toList()).stream().forEach(System.out::println);

//            Pattern.compile(regx)
//                    .matcher("string to search from here")
//                    .results()
//                    .map(MatchResult::group)
//                    .toArray(String[]::new);

            //bufferedReader.lines().filter(e-> pattern.matcher(e).res).forEach(System.out::println); ;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try {
//            Scanner readInput = new Scanner(new File("D:\\bad emals.txt"));
//            System.out.println(readInput.);
////            while (readInput.hasNextLine())
////            {
////                printMatchers(regx,readInput.nextLine(),lineNumber++);
////            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        String longString = "this is a long string containing numbers ab abc abba baba like 15 , 14.2 , 55 and emails a.alkaff@sdkjordan.com with phone nuubers 0788686870, +962788686870, 962788686870 and 788686870 ";
//        String EXAMPLE_TEST =  "Via the $ you can refer to a group. $1 is the first group, $2 the second, etc. ";
//
//       // System.out.println(EXAMPLE_TEST);
//       // String pattern = "(\\w)(\\s+)([\\.,])";
//       // System.out.println(EXAMPLE_TEST.replaceAll(pattern, "$1$3"));

      //  printMatchers("(.*\\s+)",EXAMPLE_TEST);
    }

    private static void printMatchers(String regx, String s, int lineNo) {

        Pattern  mPattern = Pattern.compile(regx);
        Matcher regxMatcher =  mPattern.matcher(s);

        while (regxMatcher.find())
        {
            System.out.printf("%-50s :%d(%d,%d)\n",regxMatcher.group(),lineNo,regxMatcher.start(),regxMatcher.end());
        }
    }

    public static void printMatchers(String regx, String text)
    {
        Pattern  mPattern = Pattern.compile(regx);
        //System.out.println(mPattern.pattern());
       Matcher regxMatcher =  mPattern.matcher(text);
      //  System.out.printf("There is/are %d group(s) \n",regxMatcher.groupCount());
       while (regxMatcher.find())
       {

           System.out.printf("%-50s :(%d,%d)\n",regxMatcher.group(),regxMatcher.start(),regxMatcher.end());

       }
//
//        regxMatcher.
    }
}
