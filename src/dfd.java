import java.util.Random;

public class dfd {
    enum Major {
        CS, CIS, MIS, SE, CE, NE
    }

    enum Degree {
        Bsc, Msc, Phd, FullProf, AssestProf
    }

    enum Title {
        Manager, Officer, DeskWorker, Supervisour
    }

    static class Person {
        private static int _id = 1;

        public Person(String n, String ssn) {
            ID = _id++;
            Name = n;
            SSN = ssn;
        }

        public String Name;
        public int ID;
        public String SSN;


        public String toString() {
            return String.format("Person [ID:%10s, Name:%15s, SSN:%6s]", ID, Name, SSN);
        }
    }

    class Student extends Person {
        public int StudentNumber;

        public double AGP;

        public Major Major;

        public Student(String n, String ssn, int number, Major major) {
            super(n, ssn);

            StudentNumber = number;
            AGP = 0;
            Major = major;
        }
    }

    class Worker extends Person {
        public Worker(String n, String ssn, double salary) {
            super(n, ssn);

            Salary = salary;
        }

        public double Salary;
    }

    class Teacher extends Worker {
        public Degree Degree;

        public Teacher(String n, String ssn, double salry, Degree deg) {
            super(n, ssn, salry);
            Degree = deg;
        }
    }

    class Employee extends Worker {
        public Title JobTitle;

        public Employee(String n, String ssn, double salary, Title title) {
            super(n, ssn, salary);
            JobTitle = title;
        }
    }


    public static void main(String[] args) {

        Person[] gathering = new Person[100];

        String name;

        for (int i = 0; i < gathering.length; i++) {
            name = String.format("%s (%s)", GenerateRandomName(), i);
            gathering[i] = new Person(name, String.valueOf(1000 + i));

            System.out.println(gathering[i]);

        }

    }


    public static String GenerateRandomName() {
        Random rand = new Random();
        int n = 3 + rand.nextInt(7);
        StringBuilder build = new StringBuilder();
        build.append(Character.toChars('A' + rand.nextInt('Z' - 'A')));
        for (int i = 1; i < n; i++) {
            build.append(Character.toChars('a' + rand.nextInt('z' - 'a')));
        }


        return build.toString();
    }
}
