package basics2.generics;

import shared.Person;

import java.util.Scanner;

public class GenericsDemo {
    static class Pair {
        // key
        private Object Key ;
        // value
        private Object Value ;

        public Object getKey() {
            return Key;
        }

        public void setKey(Object key) {
            Key = key;
        }

        public Object getValue() {
            return Value;
        }

        public void setValue(Object value) {
            Value = value;
        }

        public Pair(Object key, Object value) {
            setKey(key);
            setValue(value);
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "Key=" + Key +
                    ", Value=" + Value +
                    '}';
        }
    }

    static class GPair <K,V>
    {
        private K Key;
        private V Value;

        @Override
        public String toString() {
            return "GPair{" +
                    "Key=" + Key +
                    ", Value=" + Value +
                    '}';
        }

        public GPair(K key, V value) {
           setKey(key);
           setValue(value);
        }

        public K getKey() {
            return Key;
        }

        public void setKey(K key) {
            Key = key;
        }

        public V getValue() {
            return Value;
        }

        public void setValue(V value) {
            Value = value;
        }
    }

    public static void main(String[] args) {

        Pair p1 = new Pair(1,"Ahmed");
        GPair<Integer,String> gp1 = new GPair<>(1,"Ahmed");

        Pair p2 = new Pair("2",new Person());
        GPair<String,Person> gp2 = new GPair<>("2", new Person());

        method(p1);
        method(p2);
        //Gmethod(gp1);
        Gmethod(gp2);
        //method(gp1);

        print(10);
        print("hi");
        print(new Person());
        print(gp1);
        print(p1);
    }

    public  static void method(Pair p)
    {

    }
    public  static void Gmethod(GPair<? extends String ,?> p)
    {

    }
    public  static void Gmethod2(GPair<? extends String ,? extends  Person> p)
    {

    }

    public static void printSum(int a, int b)
    {
        System.out.println(a+b);
    }
    public static void printSum(double a, double b)
    {
        System.out.println(a+b);
    }

    public  static <T> void  print(T a)
    {
        System.out.println("a = " + a);
    }
    public  static <T extends  String> void  printSum(T a, T b)
    {
        System.out.println(a.concat(b));
    }
}
