package basics2.generics;

import jdk.jshell.spi.ExecutionControl;

import java.awt.image.ImagingOpException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class GenericArray<T> {
    private static final int INITIAL_CAPACITY = 10;

    private int size;

    private T[] array;

    public GenericArray(int capacity) {
        array = (T[]) new Object[capacity];
    }
    public void add(T t)
    {
        if(size == array.length)
            extendArray();

        array[size++] = t ;
    }

    private void extendArray() {
        T[] temp = (T[]) new Object[array.length << 1 ];                //array.length * 2
        for(int i=0;i<array.length;i++)
            temp[i] = array[i];

        array = temp ;
    }

    public void add(int index,T t)
    {
        if(index <0 || index > size)
            throw  new IndexOutOfBoundsException();

        if(size == array.length)
            extendArray(index, t);

        int i = size ;
        while (i > index) {
            array[i] = array[i - 1];
            i-- ;
        }

        array[index] = t ;
        size++ ;
    }

    private void extendArray(int index, T t) {
        T[] temp = (T[]) new Object[array.length << 1 ];                //array.length * 2
        for(int i=0;i<array.length;i++) {
            if(i< index)
                temp[i] = array[i];
            else if (i == index)
                temp[i] = t ;
            else
                temp[i] = array[i-1];
        }

        array = temp ;
    }

    public GenericArray(Class<T> c, int capacity) {
        array = (T[]) Array.newInstance(c, capacity);
    }

    public GenericArray(Class<T> c) {
        this(c, INITIAL_CAPACITY);
    }

    public GenericArray() {
        this(INITIAL_CAPACITY);
    }


    // TODO: implement the following methods

    public T removeFirst()
    {
        // TODO: remove the first element of the array, and return it
        return  removeAt(0) ;
    }

    public T removeLast()
    {
        // TODO: remove the last element of the array, and return it
        return  removeAt(size-1) ;
    }
    public boolean remove(T t)
    {
        // TODO: remove the specified element of the array, and return true if it was removed
        int inx = find(t);
        if(inx >=0)
        {
            removeAt(inx);
            return  true;
        }
        return  false ;
    }
    public T removeAt(int index)
    {
        // TODO: remove the  element at the specified index of the array, and return it
        if(index <0 || index > size)
            throw  new IndexOutOfBoundsException();

        T temp = array[index];
        int j = index ;
        while(j <size-1)
        {
            array[j]= array[j+1];
            j++ ;
        }
        size-- ;
        return  temp ;
    }
    public int removeAll(T t)
    {
        // TODO: remove the  occurrences of the element in the array, and return the number of removed elements

        Integer[] indx = findAll(t);
        if(indx == null)
            return -1 ;

        for(int i : indx)
        {
            removeAt(i);
        }
        return  indx.length ;
    }

    public boolean contains(T element)
    {
        //TODO: check if the array contains the element
        return  find(element) != -1 ;
    }
    public int find(T element)
    {
        //TODO:  Find the index of the element if exist, return -1 if it not
        for(int i=0;i<size;i++)
        {
            if(array[i].equals(element))
                return i;
        }
        return  -1 ;
    }

    public Integer[] findAll(T element)
    {
        //TODO:  Find all indexes of the element (if exist), return null if it not
        ArrayList<Integer> ids = new ArrayList<>();
        for(int i=0;i<size;i++)
        {
            if(array[i].equals(element))
                ids.add(i);
        }

        if(ids.size() == 0)
            return  null ;
        else
            return ids.toArray(new Integer[ids.size()]);
    }

    public void clear()
    {
        //TODO:  Remove all elements
        size = 0 ;
    }

    public void setValueAt(int index, T newElement)
    {
        //TODO:  change the value of the element in the specified index
        if(index <0 || index>= size)
            throw  new IndexOutOfBoundsException();

        array[index] = newElement ;
    }

    public T getItem(int index) {
        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException();
        return array[index];
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        if (size == 0)
            return "{}";
        for (int i=0;i<size;i++)
            builder.append(array[i]).append(", ");

        return builder.toString().substring(0, builder.length() - 2) + "}";

    }
}
