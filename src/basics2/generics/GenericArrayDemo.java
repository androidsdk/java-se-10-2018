package basics2.generics;

public class GenericArrayDemo {
    public static final String TABLE_NAME= "contacts";
    public static final String CONTACTS_ID= "contact_id";
    public static final String CONTACTS_NAME= "Name";
    public static final String CONTACTS_Phone= "Phone";
    public static void main(String[] args) {

        GenericArray<String> genericArray  = new GenericArray<>();
        GenericArray<String> genericArray1 = new GenericArray<>(50);
        GenericArray<String> genericArray2 = new GenericArray<>(String.class);
        GenericArray<String> genericArray3 = new GenericArray<>(String.class,50);


        System.out.println("genericArray1 = " + genericArray1);
        genericArray1.add("1");
        genericArray1.add("2");
        genericArray1.add("5");
        genericArray1.add("7");


        Double z;

        System.out.println("genericArray1 = " + genericArray1);

        genericArray1.add(2,"3");
        genericArray1.add(0,"0");
        genericArray1.add(genericArray1.size(),"10");

        System.out.println("genericArray1 = " + genericArray1);

       

        String sql = "CREATE TABLE "+TABLE_NAME+" (" +
                CONTACTS_ID +" INTEGER PRIMARY KEY, " +
                CONTACTS_NAME +" TEXT NOT NULL, " +
                CONTACTS_Phone +" text NOT NULL UNIQUE" +");";

        System.out.println("sql = " + sql);
 ;
    }
}
