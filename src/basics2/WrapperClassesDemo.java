package basics2;

import java.math.BigInteger;
import java.util.Locale;
import java.util.Random;

public class WrapperClassesDemo {

    public static void main(String[] args) {

        int pri = -130;
        Object o = new Object();

        String str = "";
        String str1 = new String("");

        Integer a1 = 0;
        Integer a2 = 0;
        Random rand = new Random();
        BigInteger big = BigInteger.ONE ;
        for (int i = 0; i < 10; i++) {
//            Integer i = 10;    // automatic boxing
            // pri = i;            // automatic unboxing

            big = big.multiply(BigInteger.probablePrime(128,rand));
            System.out.println(big.bitLength()+":"+big+"\n");
//            a1 = i;
//            a2 = i;

//            System.out.printf(Locale.getDefault(), "i:%5d, a1:%5d, a2:%5d : %s%n", i, a1, a2, (a1 == a2));

        }


        int c = a1 + a2 ;
        BigInteger bigInteger  = BigInteger.valueOf(152) ;
        BigInteger bigInteger1 = bigInteger.add(BigInteger.valueOf(154));
        pri = 1555 ;
        method(pri, pri);    // automatic boxing


//        method(pri, a1);        // 1
//        method(a1, pri);        // 4
//        method(pri, pri);       // 2
//        method(a2, a1);         // 3
    }



    public static Integer test()
    {
        return  null ;
    }


    public static void method(Integer o, Integer o1)
    {
        System.out.println("(o == o1) = " + (o == o1));
        System.out.println("(o == o1) = " + (o.equals(o1)));
        System.out.println("o = " + o);
    }

//    public static void method(int a, Integer b) {
//        System.out.println("1");
//    }
//
////    public static void method(int a, int b) {
////        System.out.println("2");
////    }
//
////    public static void method(Integer a, Integer b) {
////        System.out.println("3");
////    }
//
//    public static void method(Integer a, int b) {
//        System.out.println("4");
//    }


}

