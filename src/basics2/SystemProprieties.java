package basics2;

public class SystemProprieties {
    public static void main(String[] args) {
        for(Object key : System.getProperties().keySet())
        {
            System.out.printf("%-30s:%s%n",key,System.getProperty(key.toString()));
        }
    }
}
