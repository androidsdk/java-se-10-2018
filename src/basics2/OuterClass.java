package basics2;

import  static basics2.StaticDemo.TAG;
public class OuterClass {


    // inner classes  can access the outer class private members
    private int OuterVariable;

    private static  int OuterStaticVariable;
    public void method()
    {
        System.out.println("TA = " + TAG);
        InnerStaticClass innerStaticClass = new InnerStaticClass();
        //innerStaticClass.b ;

        InnerNonStaticClass innerNonStaticClass = new InnerNonStaticClass();
        //innerNonStaticClass.a ;

        // InnerStaticClass.staticB ;
    }

    public class InnerNonStaticClass{
        // private static int staticA ;
        private int a ;
        private void method()
        {
            // Outer static members can be accessible from the inner non static class
            OuterClass.OuterStaticVariable = 10 ;
            // from non static inner class only you can access
            // the instance fields of the outer class
             OuterVariable = 10 ;
        }
    }
    public static class InnerStaticClass {
        private static int staticB ;
        private int b ;
        private void method()
        {

            // Outer static members can be accessible from the inner  static class
            OuterClass.OuterStaticVariable = 10 ;
            // from  static inner class  you can NOT access
            // the instance fields of the outer class
            // OuterVariable = 10 ;
        }
    }


}
