package basics2;

public class InnerClassDemo {
    public static void main(String[] args) {
        OuterClass.InnerStaticClass innerStaticClass
                = new OuterClass.InnerStaticClass();


//        OuterClass.InnerNonStaticClass innerNonStaticClass
//                = new OuterClass.InnerNonStaticClass();


        OuterClass outerClassObj = new OuterClass();
        OuterClass.InnerNonStaticClass innerNonStaticClass
                = outerClassObj.new InnerNonStaticClass();

        // OR


        OuterClass.InnerNonStaticClass nonStaticClass = new OuterClass().new InnerNonStaticClass();

    }
}
