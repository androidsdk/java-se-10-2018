package basics2;

public final class FinalDemo {


    // final class : are classes that can't be extended

    // final methods : are methods that can't be overridden
    // is the opposite of abstract method, so can't to have abstract final method
    public final String finalMethod()
    {
        return "Final Method";
    }


    // final variable: is variable can be initiated once only

    // instance variable final must be instantiated at definition statement .
    final int FinalInstanceVariable = 120  ;

    public void method()
    {
        // local variable final can be instantiated  after definition  .
        final  int localFinal  ;
        localFinal = 10 ;

//        localFinal = 20 ;
//        FinalInstanceVariable = 20 ;
    }

}
