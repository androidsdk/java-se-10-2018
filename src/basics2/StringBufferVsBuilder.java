package basics2;

public class StringBufferVsBuilder {
    public static void main(String[] args) {

        StringBuilder builder = new StringBuilder("{");
        StringBuffer buffer = new StringBuffer("{");           // can be used by many users at the same time

        String str = "{";
        int[] array = {1,2,3,4,5,6,7,8,9};
        for(int i: array)
        {
            builder.append(i).append(", ");
            //str += i + ", ";
        }

        if(builder.length()<2)
            builder.append(" }");
        else
            builder.append("\b\b} ");
        str = builder.toString();
        //str += "\b\b} ";

        System.out.println("str = " + str);

    }
}
