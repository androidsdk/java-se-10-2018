package basics2.collections;

import java.util.*;

public class Collections {
    public static void main(String[] args) {

        ArrayList<String> stringArrayList = new ArrayList<>();
        System.out.println("stringArrayList = " + stringArrayList+ ", Size:"+stringArrayList.size());
        stringArrayList.add("First");
        stringArrayList.add("Second");
        stringArrayList.add("Third");
        stringArrayList.add("Fourth");
        System.out.println("stringArrayList = " + stringArrayList+ ", Size:"+stringArrayList.size());



        LinkedList<String> stringLinkedList = new LinkedList<>();
        System.out.println("stringLinkedList = " + stringLinkedList+ ", Size:"+stringLinkedList.size());
        stringLinkedList.add("First");
        stringLinkedList.add("Second");
        stringLinkedList.add("Third");
        stringLinkedList.add("Fourth");
        System.out.println("stringLinkedList = " + stringLinkedList+ ", Size:"+stringLinkedList.size());


        Vector<String> stringVector = new Vector<>();
        System.out.println("stringVector = " + stringVector+ ", Size:"+stringVector.size());
        stringVector.add("First");
        stringVector.add("Second");
        stringVector.add("Third");
        stringVector.add("Fourth");
        System.out.println("stringVector = " + stringVector+ ", Size:"+stringVector.size());

        Stack<String> stringStack = new Stack<>();
        System.out.println("stringStack = " + stringStack+ ", Size:"+stringStack.size());
        stringStack.add("First");
        stringStack.add("Second");
        stringStack.add("Third");
        stringStack.add("Fourth");
        System.out.println("stringStack = " + stringStack+ ", Size:"+stringStack.size());



        PriorityQueue<String> stringPriorityQueue = new PriorityQueue<>();
        System.out.println("stringPriorityQueue = " + stringPriorityQueue+ ", Size:"+stringPriorityQueue.size());
        stringPriorityQueue.add("First");
        stringPriorityQueue.add("Second");
        stringPriorityQueue.add("Third");
        stringPriorityQueue.add("Fourth");
        stringPriorityQueue.add("First");
        System.out.println("stringPriorityQueue = " + stringPriorityQueue+ ", Size:"+stringPriorityQueue.size());



        HashSet<String> stringHashSet = new HashSet<>();
        System.out.println("stringHashSet = " + stringHashSet+ ", Size:"+stringHashSet.size());
        stringHashSet.add("First");
        stringHashSet.add("Second");
        stringHashSet.add("Third");
        stringHashSet.add("Fourth");
        stringHashSet.add("First");
        System.out.println("stringHashSet = " + stringHashSet+ ", Size:"+stringHashSet.size());

        LinkedHashSet<String> stringLinkedHashSet = new LinkedHashSet<>();
        System.out.println("stringLinkedHashSet = " + stringLinkedHashSet+ ", Size:"+stringLinkedHashSet.size());
        stringLinkedHashSet.add("First");
        stringLinkedHashSet.add("Second");
        stringLinkedHashSet.add("Third");
        stringLinkedHashSet.add("Fourth");
        stringLinkedHashSet.add("first");
        System.out.println("stringLinkedHashSet = " + stringLinkedHashSet+ ", Size:"+stringLinkedHashSet.size());


        TreeSet<String> stringTreeSet = new TreeSet<>();
        System.out.println("stringTreeSet = " + stringTreeSet+ ", Size:"+stringTreeSet.size());
        stringTreeSet.add("first");
        stringTreeSet.add("First");
        stringTreeSet.add("Second");
        stringTreeSet.add("Third");
        stringTreeSet.add("Fourth");
        System.out.println("stringTreeSet = " + stringTreeSet+ ", Size:"+stringTreeSet.size());


        HashMap<Integer,String> hashMap = new HashMap<>();
        hashMap.put(3,"0788686870");
        hashMap.put(1,"Ahmed");
        hashMap.put(2,"a.alakff@gmail.com");
        hashMap.put(3,"07956498114");


        System.out.println("hashMap = " + hashMap);

        TreeMap<Integer,String> treeMap = new TreeMap<>();
        treeMap.put(3,"0788686870");
        treeMap.put(1,"Ahmed");
        treeMap.put(2,"a.alakff@gmail.com");
        treeMap.put(3,"07956498114");
        System.out.println("treeMap = " + treeMap);

        LinkedHashMap<Integer,String> linkedHashMap = new LinkedHashMap<>();

        linkedHashMap.put(3,"0788686870");
        linkedHashMap.put(1,"Ahmed");
        linkedHashMap.put(2,"a.alakff@gmail.com");
        linkedHashMap.put(3,"07956498114");
        System.out.println("linkedHashMap = " + linkedHashMap);


        final int  KEY_NAME = 1;
        final int  KEY_PHONE = 2;
        final int  KEY_EMAIL = 3;
        final int  KEY_PHONE2 = 4;
        
        HashMap<Integer,String> user = new HashMap<>();
        user.put(KEY_NAME,"Ahmed Alkaff");
        user.put(KEY_PHONE,"0788686870");
        user.put(KEY_EMAIL,"a.alkaff@gmail.com");
        user.put(KEY_PHONE2,"079564524");


        System.out.println("user = " + user);

        HashMap<Integer,String> user1 = new HashMap<>();
        user1.put(KEY_NAME,"Mosab ");
        user1.put(KEY_PHONE,"078981447");
        user1.put(KEY_EMAIL,"am.makawi@gmail.com");

        LinkedList<Map <Integer,String>> contacts = new LinkedList<>();
        contacts.add(user);
        contacts.add(user1);

        System.out.println("contacts = " + contacts);


        for(Map<Integer,String> m : contacts)
        {
            System.out.println("m.keySet() = " + m.keySet());
            if(m.containsKey(KEY_PHONE2))
                System.out.println(m.get(KEY_PHONE2));
        }


    }
}
