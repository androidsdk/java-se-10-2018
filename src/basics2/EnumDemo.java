package basics2;

import java.time.Month;
import java.util.Locale;

public class EnumDemo {
    static enum Gender {
        Female (2), Male (3);

        private int idex ;
        Gender(int i) {
            idex =i ;
        }

        public int getIdex() {
            return idex;
        }
    }

    static enum Month{
        January(1,31),
        February(2,29),
        March(3,31),
        April(4,30),
        May(5,31),
        June(6,30),
        July(7,31),
        August(8,31),
        September(9,30),
        October(10,31),
        November(11,30),
        December(12,31);



        private  int monthNumber;
        private  int days ;
        Month(int monthNumber, int days) {
            setDays(days);
            setMonthNumber(monthNumber);
        }

        public int getMonthNumber() {
            return monthNumber;
        }

        private void setMonthNumber(int monthNumber) {
            if(monthNumber > 0 && monthNumber <=12)
            this.monthNumber = monthNumber;
            else
                throw new IllegalArgumentException();
        }

        public int getDays() {
            return days;
        }

        private void setDays(int days) {
            if(days>= 28 && days <= 31)
                this.days = days;
            else
                throw new IllegalArgumentException();
        }

        public String getAbbreviation()
        {
            return this.name().substring(0,3);
        }
    }

    public static void main(String[] args) {
        Gender gender = Gender.Female;

        System.out.println("gender = " + gender);
        System.out.println("gender.name() = " + gender.name());
        System.out.println("Gender.Female = " + Gender.Female);
        System.out.println("Gender.Male = " + Gender.Male);
        System.out.println("Gender.Female.getIdex() = " + Gender.Female.getIdex());
        System.out.println("Gender.Male.getIdex() = " + Gender.Male.getIdex());
        for(int i=0; i< Gender.values().length;i++)
            System.out.println("i = " + i);


        for(Month m :Month.values())
        {
            System.out.printf(Locale.getDefault(),"%2d:%-10s,%-4s,%d%n",m.getMonthNumber(),m.name(),m.getAbbreviation(),m.getDays());
        }
    }
}
