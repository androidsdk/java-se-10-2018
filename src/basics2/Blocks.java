package basics2;

import java.util.Properties;

public class Blocks {

    static class Tester {

        // Called each time you create an object
        private  String tempDir  ;
        {
            if(System.getProperty("sun.desktop").startsWith("windows"))
                tempDir = "C:\\windows\\temp";
            else if(System.getProperty("sun.desktop").startsWith("mac"))
                tempDir = "/tmp";
            else
                tempDir = "/usr/temp";

            System.out.println("tempDir = " + tempDir);
            System.out.println("\"Block\" = " + "Block");
        }

        // Called once, on the on loading the class
        // class is loaded before its first use
        static {
            System.out.println("\"Static block\" = " + "Static block");
        }
    }
    public static void main(String[] args) {

        System.out.println("Hi");

        Tester t = new Tester();
        new Tester();
    }
}
