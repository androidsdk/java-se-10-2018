package basics2.exceptions;

import java.io.*;
import java.util.Scanner;

public class ExceptionWithFinally {

    public  static void readFromFile(String filepath)
    {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(filepath));
            while (scanner.hasNextLine())
            {
                System.out.println(scanner.nextLine());
            }

            // Exception happened  here

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IllegalStateException ex)
        {

        }finally {
            if(scanner != null) scanner.close();
        }


    }

    public  static void readFromFile1(String filepath)
    {
        // try with resource , since Java 7
        try (Scanner scanner = new Scanner(new File(filepath)))
        {
            while (scanner.hasNextLine())
            {
                System.out.println(scanner.nextLine());
            }

            // Exception happened  here

        } catch (FileNotFoundException | IllegalStateException e) {
            e.printStackTrace();
        }
    }

    // try with multiple resources
    public void CopyFile(String source, String destination)
    {
        try (BufferedReader reader = new BufferedReader(new FileReader(source));
            PrintWriter writer = new PrintWriter(new FileWriter(destination)))
        {

            // TODO : copy the file content, this will be done on FileIO API later

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {


        String file = "D:\\target.txt";
        readFromFile(file);
    }
}
