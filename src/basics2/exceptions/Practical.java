package basics2.exceptions;

public class Practical {

    public static void method()
    { throw new ArithmeticException("A"); }
    public  static void Method2() throws Exception {
        try
        {
            method();
            System.out.println("F");
        }catch (IllegalArgumentException ex)
        {
            System.out.println(ex.getMessage());
            System.out.println("B");
        }catch (ArithmeticException ex)
        {
            System.out.println(ex.getMessage());            // A
            System.out.println("C");                        // C
            throw new Exception("New") ;
        }catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println("D");

            throw  new MyException();
        }

        System.out.println("I");
    }
    public static void main(String[] args) {
        try {
            Method2();
            System.out.println("E");
        }catch (Exception ex)
        {
            System.out.println(ex.getMessage());            // New
            System.out.println("G");                        // G
        }
    }
}
