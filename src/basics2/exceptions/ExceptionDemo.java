package basics2.exceptions;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ExceptionDemo {

    public static Integer divide(int a, int b) {

            return a / b;                    // 1

    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x=0, y=0;
        do {
            try{
                System.out.print("X:");
                x = scanner.nextInt();
                System.out.print("Y:");
                y = scanner.nextInt();
                scanner.nextLine();
                System.out.printf(Locale.getDefault(), "%d / %d = %s%n", x, y, divide(x, y));   // 2
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
                System.out.printf(Locale.getDefault(), "%d / %d = \u221E %n", x, y);   // 2
            } catch (InputMismatchException ex) {

            }
            catch (IllegalStateException | NoSuchElementException  ex)
            {
                // multi catch
            }catch (Exception e)
            {
            }catch (Throwable t)
            {
                if(t instanceof  Error)
                {
                    // Handle errors
                }else if (t instanceof  Exception)
                {
                    if(t instanceof  RuntimeException)
                    {
                        if(t instanceof  ArithmeticException)
                        {

                        }
                    }else if(t instanceof IOException)
                    {

                    }
                }
            }
            System.out.println("Press enter to continue or Q to exit");
        } while (!scanner.nextLine().equalsIgnoreCase("q"));

        System.out.println("Thank you");
        scanner.close();


    }
}
