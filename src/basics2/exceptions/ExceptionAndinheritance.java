package basics2.exceptions;

public class ExceptionAndinheritance {

    static class Parent {

        public void method1() throws ArithmeticException {
        }

        public void method2() throws Exception {
        }

        public void method3() {
        }

    }

    static class Child extends Parent {

        // if the parent method throws an unchecked exception then you can remove it in the overridden method
        @Override
        public void method1() {
        }
        // if the parent method throws an checked exception then you can remove it in the overridden method

        @Override
        public void method2() throws ArithmeticException {
        }

        // if the parent method does not throws any exception then you can add throws to its overridden method
        // only for a unchecked exceptions

        @Override
        public void method3() throws ArithmeticException {
        }

        /// In summary : in the overridden methods you can remove the throws  or add an uncheck exception to method with no throws

        ///


        public static void main(String[] args) {
            try {
                Parent p = new Parent();
                Child c = new Child();
                Parent pc = new Child();

                p.method1();
                p.method2();
                p.method3();

                c.method1();
                c.method2();
                c.method3();

                pc.method1();
                pc.method2();
                pc.method3();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
