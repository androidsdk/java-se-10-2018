package basics2.exceptions;

public class ThrowsAndThrow {


    static  class Person {
        int age ;
    }
    public  static  void someMethod() {
        try {
            // TODO: do any thing that may cause an exception

            throw  new ArithmeticException();
        }catch (NullPointerException | IllegalArgumentException e)
        {
            // Exception is checked exception so we have to do one of :
            // 1) surround it with try catch block
            // 2 ) annotate the method with throws
            //throw  new Exception();
        }
        catch (Exception ex)
        {
            // TODO: handle the caught exception
            // throw an Exception  variable does not force to deal with check exception
            // because we have no grantee about the type of exception object
            // if line 10 is   throw  new ArithmeticException(); then no check is needed
            // but if it was like   throw  new Exception() then check will be needed
            throw  ex ;
        }catch (Throwable t)
        {
            // TODO: log the exception then throw it back
            throw t ;
        }
    }

    public static void main(String[] args) {

    }
}
